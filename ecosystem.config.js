module.exports = {
  apps : [
    {
      name        : "server",
      script      : "./server.js",
      watch       : true,
      ignore_watch : [
        "node_modules",
        "persist",
        "source",
        "source/**/*",
        "public",
        "storage",
        ".git",
        ".idea",
        ".history",
        "public/js",
        "public/qr",
        "public/uploads",
        "tmp"
      ],
      env: {
        "NODE_ENV": "development",
      },
      env_production : {
        "NODE_ENV": "production"
      }
    }
  ]
}
