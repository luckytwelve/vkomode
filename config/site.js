'use strict';

const Env = use('Env');

module.exports = {
  languages: [{
    name: 'ru',
    label_ru: 'Русский',
    label_ua: 'Російська'
  }, {
    name: 'ua',
    label_ru: 'Украинский',
    label_ua: 'Українська'
  }]
};
