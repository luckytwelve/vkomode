module.exports = {
  'text': `<i class="kt-menu__link-icon flaticon-interface-8"></i><span class="kt-menu__link-text">{{Tests.qqs.qqs}}</span>`,
  'a.href': 'Tests.qqs.list',
  'li.class': 'kt-menu__item',
  'a.class': 'kt-menu__link kt-menu__toggle',
  'permissions': ['qqs_view']
};