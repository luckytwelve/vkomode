up:
	docker-compose up

fresh:
	docker-compose stop
	docker rm briz-server || true
	docker rm briz-pg || true
	docker rmi www_briz_web || true
	docker rmi www_biz_pg || true
	docker-compose up

ssh:
	docker-compose exec melin-app bash

ssh-pg:
	docker-compose exec pg bash

theme:
	cd source/metronic_7/ && http-server

gulp:
	gulp admin

server:
	ssh -R 80:localhost:8084 serveo.net