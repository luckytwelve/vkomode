'use strict'

/** @type {import('@adonisjs/framework/src/Server')} */
const Server = use('Server')
const path = require('path');
/*
|--------------------------------------------------------------------------
| Global Middleware
|--------------------------------------------------------------------------
|
| Global middleware are executed on each http request only when the routes
| match.
|
*/
const globalMiddleware = [
  'Adonis/Middleware/BodyParser',
  'Adonis/Middleware/Session',
  'Adonis/Middleware/Shield',
  'Adonis/Middleware/AuthInit',
  'Adonis/Acl/Init',
  'Localization/Middleware'
  // 'ADM/Static',
  // '@provider:Adm/Services/AdminStatic/Middleware'
];

/*
|--------------------------------------------------------------------------
| Named Middleware
|--------------------------------------------------------------------------
|
| Named middleware is key/value object to conditionally add middleware on
| specific routes or group of routes.
|
| // define
| {
|   auth: 'Adonis/Middleware/Auth'
| }
|
| // use
| Route.get().middleware('auth')
|
*/
const namedMiddleware = {
  managerAuth: '@provider:Auth/Middleware/ManagerAuth',
  managerXHR: '@provider:Auth/Middleware/ManagerXhr',
  managerGuest: '@provider:Auth/Middleware/ManagerGuest',
  menu: '@provider:Articles/Middleware/Menu',
  session: '@provider:Shop/Middleware/Session',
  is: 'Adonis/Acl/Is',
  can: 'Adonis/Acl/Can'
};

/*
|--------------------------------------------------------------------------
| Server Middleware
|--------------------------------------------------------------------------
|
| Server level middleware are executed even when route for a given URL is
| not registered. Features like `static assets` and `cors` needs better
| control over request lifecycle.
|
*/
const serverMiddleware = [
  'Adonis/Middleware/Session',
  'FileManager/Static',
  'Adonis/Middleware/Static',
  '@provider:Adm/Services/AdminStatic/Middleware',
  'Adonis/Middleware/Cors'
]

Server
  .registerGlobal(globalMiddleware)
  .registerNamed(namedMiddleware)
  .use(serverMiddleware)