'use strict'

/*
|--------------------------------------------------------------------------
| Providers
|--------------------------------------------------------------------------
|
| Providers are building blocks for your Adonis app. Anytime you install
| a new Adonis specific package, chances are you will register the
| provider here.
|
*/

const path = require('path');


const providers = [
  '@adonisjs/framework/providers/AppProvider',
  '@adonisjs/framework/providers/ViewProvider',
  '@adonisjs/lucid/providers/LucidProvider',
  '@adonisjs/bodyparser/providers/BodyParserProvider',
  '@adonisjs/cors/providers/CorsProvider',
  '@adonisjs/shield/providers/ShieldProvider',
  '@adonisjs/session/providers/SessionProvider',
  '@adonisjs/auth/providers/AuthProvider',
  '@adonisjs/validator/providers/ValidatorProvider',
  '@adonisjs/ally/providers/AllyProvider',
  '@adonisjs/antl/providers/AntlProvider',
  '@adonisjs/session/providers/SessionProvider',
  'adonis-acl/providers/AclProvider',
  path.join(__dirname, '../modules/Adm/Services/View/Provider'),
  path.join(__dirname, '../modules/Adm/Services/Localization/Provider'),
  path.join(__dirname, '../modules/Adm/Services/AdminStatic/Provider'),
  path.join(__dirname, '../modules/Adm/Services/Adm'),
  path.join(__dirname, '../modules/Adm/Services/Notify'),
  path.join(__dirname, '../modules/Adm/Services/Menu'),
  path.join(__dirname, '../modules/Adm/Services/TableBuilder'),
  path.join(__dirname, '../modules/Adm/Services/Datatables'),
  path.join(__dirname, '../modules/Adm/Services/FileManager/provider'),
  path.join(__dirname, '../modules/Adm/Services/Thumb/Provider'),
  path.join(__dirname, '../modules/Adm/Services/Notification'),
  path.join(__dirname, '../modules/Shop/Provider'),
  path.join(__dirname, '../modules/Settings/Services/Settings')
];

/*
|--------------------------------------------------------------------------
| Ace Providers
|--------------------------------------------------------------------------
|
| Ace providers are required only when running ace commands. For example
| Providers for migrations, tests etc.
|
*/
const aceProviders = [
  '@adonisjs/lucid/providers/MigrationsProvider',
  'adonis-acl/providers/CommandsProvider',
  '@adonisjs/vow/providers/VowProvider'
]

/*
|--------------------------------------------------------------------------
| Aliases
|--------------------------------------------------------------------------
|
| Aliases are short unique names for IoC container bindings. You are free
| to create your own aliases.
|
| For example:
|   { Route: 'Adonis/Src/Route' }
|
*/
const aliases = {
  Role: 'Adonis/Acl/Role',
  Permission: 'Adonis/Acl/Permission',
}

/*
|--------------------------------------------------------------------------
| Commands
|--------------------------------------------------------------------------
|
| Here you store ace commands for your package
|
*/
const commands = [
  // 'App/Commands/PermissionSync',
  // 'App/Commands/GenerateModule'
  'App/Commands/Sitemap'
];

module.exports = { providers, aceProviders, aliases, commands }
