const { hooks } = require('@adonisjs/ignitor');
const fs = require('fs');
const path = require('path');

hooks.after.providersBooted(() => {
  const View = use('View');
  const Validator = use('Validator');
  const Database = use('Database');
  const Config = use('Config');
  const menu = Config.get('admin.menu');
  const Menu = use('ADM/Menu');
  const Env = use('Env');
  const moment = use('moment');

  // const css = fs.readFileSync(path.resolve(__dirname, '../', 'public/css/vendor.min.css'), 'utf8');
  const load = {};

  View.global('load', (_path) => {
    if (!load[_path]) {
      load[_path] = fs.readFileSync(path.resolve(__dirname, '../', _path), 'utf8')
        // .split('&quot;').join('"')
        .split('../').join('/');
    }
    return load[_path];
  });

  View.global('moment', function (...args) {
    return moment.apply(args);
  });

  View.global('Menu', function (user, __) {
    return Menu.build(menu, user, __);
  });


  View.global('DeclinationNumbers', (number, form1, form2, form3)=>{
    number = Math.abs(number);
    number %= 100;
    if (number >= 5 && number <= 20) {
      return form3;
    }
    number %= 10;
    if (number == 1) {
      return form1;
    }
    if (number >= 2 && number <= 4) {
      return form2;
    }
    return form3;
  });

  View.global('toFixed', (number)=>{
    return number.toFixed(2)
  })

  const existsFn = async (data, field, message, args, get) => {
    const value = get(data, field)
    if (!value) {
      /**
       * skip validation if value is not defined. `required` rule
       * should take care of it.
       */
      return
    }

    const [table, column] = args
    const row = await Database.table(table).where(column, value).first()

    if (!row) {
      throw message
    }
  }

  const notExistsFn = async (data, field, message, args, get) => {
    const value = get(data, field)
    if (!value) {
      /**
       * skip validation if value is not defined. `required` rule
       * should take care of it.
       */
      return
    }

    const [table, column] = args
    const row = await Database.table(table).where(column, value).first()
    if (row !== undefined) {
      console.log(row)
      throw message
    }
  }

  Validator.extend('exists', existsFn)
  Validator.extend('notExists', notExistsFn)
})
