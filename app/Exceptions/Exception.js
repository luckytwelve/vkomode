const BaseExceptionHandler = use('BaseExceptionHandler');

class ExceptionHandler extends BaseExceptionHandler {
  async handle(error, { request, response, session }) {
    if (error.name === 'ValidationException') {
      session.withErrors(error.messages).flashAll();
      await session.commit();
      response.redirect('back');
      return '';
    }
    return super.handle(...arguments);
  }
}

module.exports = ExceptionHandler;
