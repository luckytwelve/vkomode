const BaseExceptionHandler = use('BaseExceptionHandler');
const Logger = use('Logger');

const fs = require('fs');
const path = require("path");
const { promisify } = require('util');

const readFileAsync = promisify(fs.readFile);
const writeFileAsync = promisify(fs.writeFile);
const existsAsync = promisify(fs.exists);
const thumb = use('ADM/Thumb');

class ExceptionHandler extends BaseExceptionHandler {
  async handle(error, { request, response, session, antl, view }) {
    if (error.name === 'HttpException') {
      //Thumb
      const hash = path.basename(request.url()).split('.')[0];
      const jsonPath = `./public/assets/cache/${hash}`;
      try {
        if (await existsAsync(jsonPath)) {
          const json = JSON.parse(await readFileAsync(jsonPath));
          await thumb.thumb(json.path, json.options);
          fs.unlink(jsonPath, () => { });
          return response.redirect(request.url(), true);
        }
      } catch (e) {
        console.log(e)
        // return response.status(404).send();
      }


      const View = use('View');
      const Localization = use('Localization/Lang');
      View.global('__', (...args) => Localization.__.apply(null, [antl, ...args]));
      return response.status(404).send(view.render('Web.404'))
    }
    return super.handle(...arguments);
  }

  async report(error, { request }) {
    Logger.error(error.stack);
  }
}

module.exports = ExceptionHandler;