'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const moment = require('moment')

class Permission extends Model {
  // static formatDates (field, value) {
  //   if (field === 'created_at' || field === 'updated_at') {
  //     if(typeof value === 'number'){
  //       return value
  //     } else {
  //       return moment().utcOffset(0).unix()
  //     }
  //   }
  //   return super.formatDates(field, value)
  // }

  static get traits () {
      return [
        '@provider:Adonis/Acl/HasRole',
      ]
  }
}

module.exports = Permission
