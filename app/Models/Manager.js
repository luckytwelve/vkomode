'use strict'

const Hash = use('Hash')
const Model = use('Model')


const moment = require('moment');

class Manager extends Model {
  static boot () {
    super.boot()
    this.addHook('beforeSave', async (managerInstance) => {
      if (managerInstance.dirty.password) {
        managerInstance.password = await Hash.make(managerInstance.password)
      }
    })
  }

  tokens () {
    return this.hasMany('App/Models/Token')
  }

  static formatDates (field, value) {
    if (field === 'created_at' || field === 'updated_at') {
      if(typeof value === 'number'){
        return value
      } else {
        return moment().utcOffset(0).unix()
      }
    }
    return super.formatDates(field, value)
  }

  static get traits () {
    return [
      '@provider:Adonis/Acl/HasRole',
      '@provider:Adonis/Acl/HasPermission'
    ]
  }
}

module.exports = Manager
