'use strict'

const Model = use('Model');

class Image extends Model {
  static get table() {
    return 'images';
  }
  tags () {
    return this.hasMany('App/Models/Tags', 'id', 'entity_id')
  }
}

module.exports = Image;
