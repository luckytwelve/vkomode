'use strict'

const { Command } = require('@adonisjs/ace')
const SiteMap = use('Shop/Services/SiteMap');

class Sitemap extends Command {
  static get signature () {
    return 'sitemap:generate'
  }

  static get description () {
    return 'Generate site sitemap.xmp file'
  }

  async handle (args, options) {
    await SiteMap.generate();
    this.info('Generated.')
  }
}

module.exports = Sitemap
