const ADM = require('../../adm/adm');


ADM.modules.set('users', {
  tableSelector: '#users',

  // events: {
  //   'click .checkAllGroups': 'checkAllGroups',
  // },

  // checkAllGroups: function(e) {
  //   e.preventDefault();
  //   $('.checkbox :checkbox', $(this)).each(function() {this.checked = true;});
  //   $.uniform.update();
  // },

  ReloadTable: function( response, module){
    $(module.tableSelector).DataTable().ajax.reload(null, false);
  },

  // AfterFormSend: function( response, module ){
  //   if ( !response.notification ) return;
  //   if (response.notification.type == 'success'){
  //     $('.modal').modal('hide');
  //     $(module.tableSelector).DataTable().ajax.reload(null, false);
  //   }
  // },

  init: function () {
    this.table = $(this.tableSelector).DataTable({
      processing: true,
      serverSide: true,

      ajax: {
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: ADM.managerPath + '/users/list',
        type: 'POST',
        dataType: 'json',
      },

      columns: [
        {data: 'id', name: 'users.id'},
        {data: 'username', name: 'users.login', orderable: true, searchable: true},
        {data: 'email', name: 'users.email', orderable: true, searchable: true},
        {data: 'last_name', name: 'users.last_name', orderable: true, searchable: true},
        {data: 'first_name', name: 'users.first_name', orderable: true, searchable: true},
        {
          data: 'actions', 
          name: 'actions', 
          orderable: false, 
          searchable: false,
          render: ( data, type, row, meta ) => {
            return `
            <button class="btn btn-secondary dropdown-toggle" type="button" id="usersrrow" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Dropdown button
            </button>
            <div class="dropdown-menu" aria-labelledby="usersrrow">
              <a class="dropdown-item" href="${ADM.managerPath}/users/${row.id}/edit" data-toggle="kt-tooltip" title="Tooltip title" data-placement="right" data-skin="dark" data-container="body">Edit</a>
              <a class="dropdown-item" href="${ADM.managerPath}/users/${row.id}/remove">Remove</a>
              <a class="dropdown-item" href="#" data-toggle="kt-tooltip" title="Tooltip title" data-placement="left">Block</a>
            </div>
            `;
          }}
      ],

      // initComplete: function () {
      // },

      // drawCallback: function () {
      //   $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
      // },

      // preDrawCallback: function() {
      //   $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
      // }
    });
  }
});
