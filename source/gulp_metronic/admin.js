const gulp = require('gulp');
const concat = require('gulp-concat');
const order = require('gulp-order');
const uglify = require('gulp-uglify');
const args = require('get-gulp-args')();
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const debug = require("gulp-debug");
const less = require('gulp-less');
const browserify = require("browserify");
const babelify = require("babelify");
const source = require("vinyl-source-stream");
const fs = require('fs');

const ADMIN_PATH  = 'source';
const LIBS_DEST_PATH  = 'public/admin/js';
const MODULES_DEST_PATH = 'public/admin/js';
const THEME_BASE_PATH       = 'source/metronic_theme/theme/classic/assets';
const THEME_DEST_PATH       = 'public/admin';


gulp.task('admin:libs', function () {
  return gulp.src(`${ADMIN_PATH}/admin/libs/**/*`)
    .pipe(order([
      'jquery.min.js',
      'jquery.validate.js',
      'popper.min.js',
      '/**/*',
      '/*',
      'bootstrap-notify.js'
    ]))
    .pipe(concat('libs.min.js'))
    .pipe(gulp.dest(LIBS_DEST_PATH));
});

gulp.task('admin:theme:js', function () {
  return gulp.src(`${ADMIN_PATH}/admin/theme/**/*`)
  .pipe(order([
    'scripts.bundle.js',
    'theme.js',
    '**/*',
    '*'
  ]))
    .pipe(concat('theme.min.js'))
    .pipe(gulp.dest(LIBS_DEST_PATH));
});


gulp.task('admin:modules', function () {
  var b = browserify({
    extensions: [".jsx", ".js"],
    debug: false
  });

  fs.readdirSync(`${ADMIN_PATH}/admin/modules`).filter((file) => {
    return (file.slice(-3) === '.js');
  }).forEach((file) => {
    b.add(`${ADMIN_PATH}/admin/modules/${file}`);
  });

  return b.transform("babelify", {
      presets: [
        "es2015", "react", "stage-0", "stage-1", "stage-2", "stage-3"],
      plugins: [
        "syntax-decorators"
      ]
    })
    .bundle()
    .pipe(source('modules.min.js'))
    .pipe(gulp.dest(MODULES_DEST_PATH));
});

gulp.task('admin:theme:css:extends', function() {
  return gulp.src(`${ADMIN_PATH}/admin/css/**/*.css`)
    .pipe(concat('extras.min.css'))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest(THEME_DEST_PATH + '/css'));
});

gulp.task('admin:theme:css:bundle', function() {
  
  return gulp.src([
    //'pages/login/login-1.css',
    'css/demo1/style.bundle.css',
    'css/demo1/skins/header/base/light.css',
    'css/demo1/skins/header/menu/light.css',
    'css/demo1/skins/brand/dark.css',
    'css/demo1/skins/aside/dark.css',
    'vendors/custom/datatables/datatables.bundle.css'
  ].map(c=>`${THEME_BASE_PATH}/${c}`))
    //.pipe(debug({title: 'unicorn:'}))
    //.pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(concat('theme.min.css'))
    .pipe(gulp.dest(THEME_DEST_PATH + '/css'));
});

gulp.task('admin:theme:css', [
  'admin:theme:css:extends',
  'admin:theme:css:bundle'
], function() {});


if (args.production === undefined) {
  gulp.watch(`${ADMIN_PATH}/adm/**/*.js`, ['admin:modules', 'admin:theme:js']);
  gulp.watch(`${ADMIN_PATH}/admin/modules/*.js`, ['admin:modules']);
  gulp.watch(`${ADMIN_PATH}/admin/libs/*.js`, ['admin:libs']);
  gulp.watch(`${ADMIN_PATH}/admin/css/**/*`, ['admin:theme:css']);
}

gulp.task('admin', [
	'admin:theme:css',
	'admin:libs',
	//'adm',
  'admin:modules',
  'admin:theme:js'
]);