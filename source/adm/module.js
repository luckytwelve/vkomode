var Module = {
  name:'',
  el: {},
  _init: function ( debug ) {
    if(debug === undefined) debug = false;
    this.destroy();
    if (this.init) this.init( this.el() );
    if (this.events) this._eventInit( debug );
    this.el().attr('data-instance',this);
  },
  init: function ( debug ) {
  },
  events: function () {
  },
  _parseCommand: function( command ){
    var command = command.split(':');
    var event = command[0];
    command.splice(0,1);
    var selector = command.join(':');
    return {event:event, selector:selector};
  },
  _eventsDestroy: function(){
    var module = this;
    for(var key in this.events){
      let command = this._parseCommand( key );
      $(document).off(command.event, '[data-module='+module.name+'] ' +command.selector);
    }
  },
  _eventInit: function( debug ){
    var module = this;
    for(var key in this.events){
      let command = this._parseCommand( key );
      if ( debug ) console.log('Model: '+ module.name + '  Attach event "'+command.event+'" on: [data-module='+module.name+']' + command.selector    );

      (function($,command, key, module){
        var _function = module.events[key] === 'function'? module.events[key] : module[module.events[key]] ;
        $(document).on(command.event, '[data-module='+module.name+'] ' +command.selector, function(e){
          var result = _function.apply(this, [e,module]) ;
          return result;
        });
      })(jQuery, command, key, module)

    }
  },

  destroy: function(){
    var module = this;
    for(var key in this.events){
     let command = this._parseCommand( key );
      $(document).off(command.event, '[data-module='+module.name+'] ' +command.selector);
    }
  }
};

module.exports = Module;