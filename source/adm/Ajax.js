/*
    AJAX default settings
*/
const notification = require('./Notification');

$.ajaxSetup({
  statusCode: {
    401: () => {
      window.location.href = ADM.managerPath + '/admin';
    },
    403: () => {
      notifications.error({ 
        message: "You are not have permissions for this action." 
      });
    },
    404: () => {
      notification.error({ 
        message: "Page not found." 
      });
    }
  },
  error: function () {
    notification.error({ 
      message: "Something was wrong." 
    });
  }
});

const Ajax = function (params) {
  let _success = params.success;
  delete params.success;

  $('.loader').fadeIn(300);
  params = $.extend(true, {}, {
    type: 'post',
    dataType: 'json',
    cache: false,
    url: '',
    error: (request) => {
      $('.loader').fadeOut(300);
      if (parseInt(request.status) == 401) {
        window.location.href = ADM.managerPath + '/account/login';
      } else {
        ADM.RequestParse(request);
      }
    },
    success: function (request, codeMessage, xhr) {
      
      $('.loader').stop(true, true).fadeOut(500);
      ADM.RequestParse(request);
      if (_success) {
        if (typeof _success === 'function') {
          _success(request);
        } else if (_success.indexOf('::')) {
          let _tmp = _success.split('::');
          _success = ADM.modules.get(_tmp[0])[_tmp[1]];
          _success(request, ADM.modules.get(_tmp[0]));
        }
      }

    }
  }, params);
  $.ajax(params);
};
module.exports = Ajax;
