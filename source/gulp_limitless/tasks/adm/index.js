const gulp = require('gulp');
const concat = require('gulp-concat');
const order = require('gulp-order');
const uglify = require('gulp-uglify');
const args = require('get-gulp-args')();
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const less = require('gulp-less');
const browserify = require("browserify");
const babelify = require("babelify");
const source = require("vinyl-source-stream");
const fs = require('fs');

const ADM_BASE_PATH = 'source/adm/';
const ADM_DEST_PATH = 'public/admin/js';

gulp.task('adm', function () {
  return browserify({
    extensions: [".js"],
    debug: false
  }).add([
    `${ADM_BASE_PATH}/_ajax.js`,
    `${ADM_BASE_PATH}/_modal.js`,
    `${ADM_BASE_PATH}/_modules.js`,
    `${ADM_BASE_PATH}/_notifications.js`,
    `${ADM_BASE_PATH}/_template.js`,
    `${ADM_BASE_PATH}/_ui.js`,
    `${ADM_BASE_PATH}/adm.js`,
    `${ADM_BASE_PATH}/stylizer.js`
  ]).transform("babelify", {
    presets: ["es2015", "stage-0", "stage-1", "stage-2", "stage-3"],
    plugins: ["syntax-decorators"]
  })
    .bundle()
    .pipe(source('core.min.js'))
    .pipe(gulp.dest(ADM_DEST_PATH));
});

if (args.production === undefined) {
  gulp.watch(`${ADM_BASE_PATH}`, ['adm']);
}
