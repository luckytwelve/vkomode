const gulp = require('gulp');
const concat = require('gulp-concat');
const order = require('gulp-order');
const uglify = require('gulp-uglify');
const args = require('get-gulp-args')();
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const less = require('gulp-less');
const browserify = require("browserify");
const babelify = require("babelify");
const source = require("vinyl-source-stream");
const fs = require('fs');

const COMPONENTS_BASE_PATH  = 'source/admin/libs/**/*';
const COMPONENTS_DEST_PATH  = 'public/admin/js';

const ADM_MODULES_BASE_PATH = 'source/admin/modules';
const ADM_MODULES_DEST_PATH = 'public/admin/js';

const THEME_BASE_PATH       = 'source/template/assets';
const THEME_DEST_PATH       = 'public/admin';


gulp.task('admin:libs', function () {
  return gulp.src(COMPONENTS_BASE_PATH)
    .pipe(order([
      'jquery.min.js',
      'bootstrap.min.js',
      'blockui.min.js',
      'uniform.min.js',
      'moment.min.js',
      'picker.js',
      'picker.date.js',
      'picker.time.js',
      'theme.js',
      '**/*',
      '*',
    ]))
    //.pipe(uglify())
    .pipe(concat('libs.min.js'))
    .pipe(gulp.dest(COMPONENTS_DEST_PATH));
});


gulp.task('admin:modules', function () {
  if (args.production === undefined) {
    gulp.watch(ADM_MODULES_BASE_PATH + '/*.js', ['admin:modules']);
  }

  var b = browserify({
    extensions: [".jsx", ".js"],
    debug: false
  });

  fs.readdirSync(ADM_MODULES_BASE_PATH).filter((file) => {
    return (file.slice(-3) === '.js');
  }).forEach((file) => {
    b.add(`${ADM_MODULES_BASE_PATH}/${file}`);
  });

  return b.transform("babelify", {
      presets: [
        "es2015", "react", "stage-0", "stage-1", "stage-2", "stage-3"],
      plugins: [
        "syntax-decorators"
      ]
    })
    .bundle()
    .pipe(source('modules.min.js'))
    .pipe(gulp.dest(ADM_MODULES_DEST_PATH));
});

gulp.task('admin:theme:css:extras', function() {
  return gulp.src(`${THEME_BASE_PATH}/css/extras/**/*.css`)
    .pipe(concat('extras.min.css'))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest(THEME_DEST_PATH + '/css'));
});

gulp.task('admin:theme:css:less', function() {
  return gulp.src(THEME_BASE_PATH + '/less/*.less')
    .pipe(less())
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(rename({suffix: ".min"}))
    .pipe(gulp.dest(THEME_DEST_PATH + '/css'));
});

gulp.task('admin:theme:css', [
  'admin:theme:css:extras',
  'admin:theme:css:less'
], function() {});

// if (args.production === undefined) {
//   gulp.watch(`${THEME_BASE_PATH}/js/**/**/**/*`, ['admin:theme:js']);
// }

if (args.production === undefined) {
  gulp.watch(`${THEME_BASE_PATH}/css/**/**/**/**/**/**/*`, ['admin:theme:css']);
}

gulp.task('admin', [
	'admin:theme:css',
	'admin:libs',
	'adm',
	'admin:modules'
]);