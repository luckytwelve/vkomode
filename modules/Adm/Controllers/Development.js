'use strict'
const { validate } = use('Validator');
const Notify = use('ADM/Notify');
const View = use('View');
const Route = use('Route');
class AccountController {
  async ui({ request, response, auth, view, __ }) {
    View.global('breadcrumbs', [
      { name: __('admin.home'), url: Route.url('admin.dashboard.index') },
      { name: __('admin.development') },
      { name: __('admin.ui'), url: Route.url('admin.roles.list') }
    ]);

    return view.render('Adm.development.ui');
  }
}

module.exports = AccountController;
