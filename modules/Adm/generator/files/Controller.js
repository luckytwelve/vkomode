const moment = require('moment');

const Route = use('Route');
const View = use('View');
const Datatables = use('ADM/Datatables');
const TableBuilder = use('ADM/TableBuilder');
const Database = use('Database');
const { validate } = use('Validator');
const __US_NAME__ = use('__MODULE__/Models/__US_NAME__');
const Notify = use('ADM/Notify');


class __UP_NAME__Controller {
  async index({ view, __, auth }) {
    const permissions = await auth.user.getPermissions();

    const table = new TableBuilder('__LP_NAME__');

    table.setName(__('__MODULE__.__LP_NAME__.list'));

    if (permissions.indexOf('__LP_NAME___create') !== -1) {
      table.setButtons([
        [`<a href="${Route.url('__MODULE__.__LP_NAME__.edit')}" class="pull-right btn btn-success">${__('Adm.admin.create')}</a>`]
      ]);
    }

    table.setColums([
      { title: '#', width: '1%' },
      { title: __('Adm.admin.actions'), width: '1%' }
    ]);

    View.global('breadcrumbs', [
      { name: __('Adm.admin.home'), url: Route.url('admin.dashboard.index') },
      { name: __('__MODULE__.__LP_NAME__.__LP_NAME__'), url: Route.url('__MODULE__.__LP_NAME__.list') }
    ]);

    return view.render('__MODULE__.__LP_NAME__.index', {
      table: table.build()
    });
  }

  async list({ request, response }) {
    const query = Database
      .select('__TABLE__.id', __FIELDS__)
      .from('__TABLE__');

    const table = new Datatables(query, request);
    const res = await table.make();
    return response.json(res);
  }

  async edit({ response, params, __ }) {
    const { id } = params;
    const data = await __US_NAME__.find(Number(id));

    if (id && !data) {
      return response.json(Notify.error('Not found', {}));
    }

    return response.json({
      modal: {
        title: id ? __('__MODULE__.__LP_NAME__.edit') : __('__MODULE__.__LP_NAME__.create_new'),
        content: View.render('__MODULE__.__LP_NAME__.form', { data }),
        cancel: __('Adm.admin.cancel'),
        submit: __('Adm.admin.save')
      },
      success: true
    });
  }

  async save({ request, response }) {
    const input = request.all();

    // const rules = {
    //   login: `min:3|max:64|unique:users,login,id,${isEdit ? input.id : ''}`,
    //   email: `required|email|unique:users,email,id,${isEdit ? input.id : ''}`,
    //   first_name: 'required|min:3|max:255',
    //   last_name: 'required|min:3|max:255',
    //   password: `${isEdit ? '' : 'required|'}min:6|max:64|same:repassword`
    // }

    // const validation = await validate(input, rules);
    // if (validation.fails()) {
    //   return response.json(Notify.error(validation.messages()[0].message, {}))
    // }

    let __LS_NAME__ = {};

    if (!input.id) {
      __LS_NAME__ = new __US_NAME__();
    } else {
      __LS_NAME__ = await __US_NAME__.find(Number(input.id));
      if (!__LS_NAME__) {
        return response.json(Notify.error('__US_NAME__ not found', {}))
      }
    }

    await __LS_NAME__.save();

    return response.json(Notify.success('Saved', {}));
  }


  async delete({ request, response, params }) {
    const { id } = params;
    const __LS_NAME__ = await __US_NAME__.find(Number(id));

    if (!__LS_NAME__) {
      return response.json(Notify.error('Something went wrong. __LS_NAME__ not found', {}));
    }

    if (await __LS_NAME__.delete()) {
      return response.json(Notify.success('Deleted', {}));
    }
    return true;
  }
}

module.exports = __UP_NAME__Controller;
