'use strict'

const { Command } = require('@adonisjs/ace');
const Permission = use('App/Models/Permission');
const Role = use('App/Models/Role');
const User = use('App/Models/User');
const Config = use('Config');
const Database = use('Database')

class PermissionSync extends Command {
  static get signature() {
    return 'module:sync';
  }

  static get description() {
    return 'Sync permissions from modules'
  }

  async handle(args, options) {
    const adminRole = await Role.find(1);
    const existedPermissions = (await Permission.all()).toJSON();
    const permissions = [].concat.apply([], Object.entries(Config.get('admin.permissions')).map(g => g[1]));

    for (const permission of permissions) {
      const exist = existedPermissions.find(p => p.slug === permission.slug)

      console.log(permission.slug)

      if (!exist) {
        // const existNew = await Permission.findBy({slug: permission.slug });
        // if (existNew){
          
        // }
        await Permission.create(permission);
      } else {
        await Permission.query()
          .where('slug', permission.slug)
          .update(permission)
      }
    }

    // Deleted not existed slugs
    await Permission.query()
      .whereNotIn('slug', permissions.map(p => p.slug))
      .delete();
    
    // Attach all permissions to admin user
    const { rows } = await Database.raw(`SELECT id FROM roles WHERE name IN ('admin', 'manager');`);
    const { rows:users } = await Database.raw(`SELECT user_id FROM role_user WHERE role_id IN (${rows.map(r => r.id).join(',')});`);

    for (let i = 0; i < users.length; i += 1){
      const user = await User.find(users[i].user_id);
      await user.permissions().detach(existedPermissions.map(p => p.id));
      await user.permissions().attach((await Permission.all()).toJSON().map(p => p.id));
    }
    
    
    // Close DB to stop command execution
    Database.close();
    return true;
  }
}

module.exports = PermissionSync
