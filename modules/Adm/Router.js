const Route = use('Route');

Route.group(() => {
  // Development
  Route
    .any('development/ui', '@provider:Adm/Controllers/Development.ui')
    .as('admin.development.ui')
    .middleware(['managerXHR', 'managerAuth', 'can:development_ui']);
}).prefix('admin');

Route.any('test-s', () => {
  const Event = use('Event');
  Event.fire('test', { data: 'hello' });
  return 'oo'
});
