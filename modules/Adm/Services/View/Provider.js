/* eslint
global-require: 0
no-underscore-dangle: 0
*/

'use strict'

const { ServiceProvider } = require('@adonisjs/fold');
const fs = require('fs');

class Provider extends ServiceProvider {
  register() {
    const Loader = require('./LoaderEdge.js');
    const View = use('View');
    View.engine._loader = new Loader(View.engine._loader._viewsPath, View.engine._loader._presentersPath);

    this.app.bind('ADM/Loader', () => {
      return {
        addPath: (path, scope) => {
          View.engine._loader.addViewsPath(path, scope);
        }
      };
    });
  }

  boot() {
  }
}
module.exports = Provider;
