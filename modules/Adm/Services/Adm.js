/* eslint
global-require: 0
no-underscore-dangle: 0
*/

'use strict'

const { ServiceProvider } = require('@adonisjs/fold');
const fs = require('fs');
const path = require('path');
const requireAll = require('require-all')
const _ = require('lodash');

const modulesFolder = path.resolve(__dirname, '../../');
const merge = require('deepmerge')

const modules = fs.readdirSync(modulesFolder, { withFileTypes: true })
  .filter(dirent => dirent.isDirectory())
  .map(dirent => dirent.name);

class Provider extends ServiceProvider {
  register() {
    const loader = use('ADM/Loader');
    const config = use('Config');
    const Localization = use('Localization/Lang');


    for (let m = 0; m < modules.length; m += 1) {
      const moduleName = modules[m];
      const modulePath = path.resolve(__dirname, '../../', moduleName);

      // Autoload -> @provider:Modules/../../..
      this.app.autoload(modulePath, moduleName);

      // Register Views
      loader.addPath(path.resolve(modulePath, 'resources/views'), moduleName);

      // Set up configs
      if (fs.existsSync(path.resolve(modulePath, 'Configs/'))) {
        const moduleMenu = requireAll(path.resolve(modulePath, 'Configs'));
        // console.log(moduleName, moduleMenu)
        config._config = merge(
          config._config,
          moduleMenu,
         // { arrayMerge: (a, b, options) => b }
        );
      }

      // Register Commands
      // if (fs.existsSync(path.resolve(modulePath, 'Commands'))) {
      //   const commands = fs.readdirSync(path.resolve(modulePath, 'Commands'));
      //   for (let i = 0; i < commands.length; i += 1) {
      //     const { name, base } = path.parse(commands[i]);
      //     this.app.bind(`${moduleName}/Commands/${name}`, () => require(path.resolve(modulePath, 'Commands', base)));
      //   }
      // }

      // Register Models
      // if (fs.existsSync(path.resolve(modulePath, 'Models'))) {
      //   const models = fs.readdirSync(path.resolve(modulePath, 'Models'));
      //   for (let i = 0; i < models.length; i += 1) {
      //     const { modelName } = path.parse(models[i]);
      //     // const nameCapitalized = modelName.charAt(0).toUpperCase() + modelName.slice(1);
      //     // this.app.bind(`${moduleName}/Models/${modelName}`, () => {
      //     //   return require(path.resolve(modulePath, 'Models', models[i]));


      //     // // const { base, name } = path.parse(models[i]);
      //     // // this.app.bind(`${moduleName}/Models/${name}`, () => {
      //     // //   return require(path.resolve(modulePath, 'Models', base));
      //     // });
      //   }
      // }

      if (fs.existsSync(path.resolve(modulePath, 'resources/locales'))) {
        const translations = requireAll({
          dirname: path.resolve(modulePath, 'resources/locales'),
          filters: /(.*)\.json$/
        });

        const normalizedTranslation = {}
        for (const lang in translations) {
          if (translations.hasOwnProperty(lang)) {
            normalizedTranslation[lang] = {
              [moduleName]: translations[lang]
            }
          }
        }
        Localization.set(normalizedTranslation);
      }
    }
  }

  boot() {
    const Response = use('Adonis/Src/Response');
    Response.macro('sendError', function (value) {
      if (typeof value === 'string') {
        this.status(422).send({
          success: false,
          message: value
        });
      } else {
        this.status(422).send({
          success: false,
          ...value
        });
      }
    });

    const crypto = require('crypto');
    const ace = require('@adonisjs/ace');
    const Server = this.app.use('Adonis/Src/Server');
    const View = use('View');
    const Env = use('Env');
    const Config = use('Config');

    View.global('config', (key, def) => {
      return Config.get(key, def);
    });

    View.global('env', (variable) => {
      return Env.get(variable);
    });


    View.global('md5', (variable) => {
      return crypto.createHash('md5').update(variable).digest("hex");
    });



    View.global('currentYear', () => {
      return new Date().getFullYear();
    });

    for (let m = 0; m < modules.length; m += 1) {
      const moduleName = modules[m];
      const modulePath = path.resolve(__dirname, '../../', moduleName);
      const routerPath = path.resolve(__dirname, '../../', moduleName, 'Router.js');
      if (fs.existsSync(routerPath)) {
        require(routerPath);
      }

      // Listeners
      if (fs.existsSync(path.resolve(modulePath, 'Listeners/'))) {
        requireAll(path.resolve(modulePath, 'Listeners'));
      }

      const routesPath = path.resolve(__dirname, '../../', moduleName, 'routes.js');
      if (fs.existsSync(routesPath)) {
        require(routesPath);
      }
      // Register commands
      if (fs.existsSync(path.resolve(modulePath, 'Commands'))) {
        const commands = fs.readdirSync(path.resolve(modulePath, 'Commands'));
        for (let i = 0; i < commands.length; i += 1) {
          const { name, base } = path.parse(commands[i]);
          ace.addCommand(`${moduleName}/Commands/${name}`)
        }
      }

      // Register middlewares
      // if (fs.existsSync(path.resolve(modulePath, 'Middleware'))) {
      //   const commands = fs.readdirSync(path.resolve(modulePath, 'Middleware'));
      //   for (let i = 0; i < commands.length; i += 1) {
      //     const { name, base } = path.parse(commands[i]);

      //     Server.registerNamed({
      //       [`${moduleName}/${name}`]: 'Adonis/Middleware/Validator'
      //     });
      //   }
      // }
    }
  }
}
module.exports = Provider;
