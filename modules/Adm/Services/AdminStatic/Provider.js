'use strict'
const { ServiceProvider } = require('@adonisjs/fold');

class AdminStatic extends ServiceProvider {
  register() {
    // this.app.bind('ADM/Static', () => {
    //   return require('./Middleware')();
    // });
    const Exception = use('Exception');

    Exception.handle('HttpException', async (error, { request, response, session }) => {
      
      // const hash = path.basename(request.url()).split('.')[0];
      // const jsonPath = `./public/assets/cache/${hash}`;

      // try {
      //   if (await existsAsync(jsonPath)) {
      //     const json = JSON.parse(await readFileAsync(jsonPath));
      //     await thumb.thumb(json.path, json.options);
      //     fs.unlink(jsonPath, () => { });
      //     return response.redirect(request.url(), true);
      //   }
      // } catch (e) {
      //   return response.status(404).send();
      // }
      // return true;
    });

  }

  boot() {

  }
}

module.exports = AdminStatic;
