'use strict'

const serveStatic = require('serve-static');
const path = require('path');
const Helpers = use('Helpers');

const defaultConfig = {};
let staticServer = null;

class Static {
  async handle({ request, response }, next) {
    if (!staticServer) {
      staticServer = Helpers.promisify(serveStatic(`${Helpers.appRoot()}/modules/Adm/resources/assets/themes/metronic/assets`, {
        fallthrough: false,
        dotfiles: 'ignore',
        etag: true,
        extensions: false
      }));
    }

    if (['GET', 'HEAD'].indexOf(request.method()) === -1) {
      return next();
    }

    if (request.request.url.indexOf('/admin/assets') !== 0) {
      return next();
    }

    request.request.url = request.request.url.replace('/admin/assets', '');
    try {
      await staticServer(request.request, request.response);
    } catch (error) {
      if (error.status === 404) {
        return next();
      }
      error.message = `${error.message} while resolving ${request.url()}`
      throw error;
    }
  }
}

module.exports = new Static();
