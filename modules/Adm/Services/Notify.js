const { ServiceProvider } = require('@adonisjs/fold')

class Notify extends ServiceProvider {
  register() {
    this.app.singleton('ADM/Notify', () => {
      return class Notify {
        static success(content, data = {}) {
          data['type'] = 'success';
          data['text'] = content;
          return Notify.response(data);
        }

        static info(content, data = {}) {
          data['type'] = 'info';
          data['text'] = content;
          return Notify.response(data);
        }

        static warning(content, data = {}) {
          data['type'] = 'warning';
          data['text'] = content;
          return Notify.response(data);
        }

        static error(content, data = {}) {
          data['type'] = 'error';
          data['text'] = content;
          return Notify.response(data);
        }

        static response(data) {
          data['text'] = Notify.parse(data['text'], data);
          return { notification: data };
        }
        static parse(template, fields) {
          if (typeof template === 'function') {
            template = template();
          } else {
            for (let i in fields) {
              if (fields.hasOwnProperty(i)) {
                template = template.replace(i, fields[i]);
              }
            }
          }
          return template;
        }
      }
    })
  }
}
module.exports = Notify;
