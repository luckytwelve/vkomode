'use strict';

const _ = require('lodash');

class Locale {
  async handle({ request, antl, view, session }, next) {
    const { lang } = request.all();

    const View = use('View');
    const Localization = use('Localization/Lang');
    const config = use('Config');
    const Env = use('Env');

    // const locales = antl.availableLocales();
    const locales = config.get('admin.general.locales');

    if (lang && locales.filter(l => l.slug === lang.toLowerCase()).length) {
      session.put('locale', lang);
      antl.switchLocale(lang);
    } else {
      const currentLocal = session.get('locale');
      if (currentLocal) {
        antl.switchLocale(currentLocal);
      } else {
        antl.switchLocale(config.get('admin.general.defaultLocale'));
      }
    }

    /** Overide class method */
    antl.get = function get(key, defaultValue = null) {
      const [group, ...parts] = key.split('.');
      const localeNode = [this._locale, group, ...parts];
      const fallbackKey = this._messages['*'] ? '*' : 'fallback';
      const fallbackNode = [fallbackKey, group, ...parts];
      return _.get(this._messages, localeNode, _.get(this._messages, fallbackNode, defaultValue));
    };

    View.global('__', (...args) => Localization.__.apply(null, [antl, ...args]));

    antl._messages = _.merge({}, Localization.getStore(), antl._messages);
    const messages = antl._messages[antl.currentLocale()];


    view.share({ localisation: JSON.stringify(messages) });
    view.share({ lang: locales.find(l => l.slug === antl.currentLocale()) });
    view.share({ locales });
    await next();
  }
}
module.exports = Locale;
