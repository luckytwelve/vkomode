'use strict'
const { ServiceProvider } = require('@adonisjs/fold');


class Locale extends ServiceProvider {
  register() {
    this.app.bind('Localization/Middleware', () => {
      const Middleware = require('./Middleware');
      return new Middleware();
    });

    const Localization = require('./Localization');
    const localization = new Localization();

    this.app.bind('Localization/Lang', () => {
      return localization;
    });
  }

  boot() {
    const HttpContext = this.app.use('Adonis/Src/HttpContext');
    const Localization = this.app.use('Localization/Lang');

    HttpContext.getter('__', function () {
      return (...args) => {
        return Localization.__.apply(null, [this.antl, ...args])
      }
    }, true);
  }
}
module.exports = Locale;
