const { ServiceProvider } = require('@adonisjs/fold');

let io = null;

class Notification extends ServiceProvider {
  register() {
    this.app.bind('Adm/Notify', () => {
      return {
        send: (data, users) => {
          if (users) {
            users.forEach((userId) => {
              io.sockets.in(`user-${userId}`).emit('event', data);
            });
          } else {
            io.emit('message', data);
          }
        },
        io: () => io
      };
    });
  }

  boot() {
    const server = use('Adonis/Src/Server');
    io = use('socket.io').listen(server.getInstance());

    io.on('connection', (socket) => {
      socket.join(`user-${socket.handshake.query.userId}`);
      // socket.on('message', console.log);
      // socket.on('event', console.log);
      // socket.on('disconnect', console.log);
    });
  }
}

module.exports = Notification;
