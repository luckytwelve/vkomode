/* eslint func-names: ["error", "never"] */
/* eslint prefer-arrow-callback: 0 */

const gulp = require('gulp');
const concat = require('gulp-concat');
const order = require('gulp-order');
const uglify = require('gulp-uglify');
const args = require('get-gulp-args')();
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
//const debug = require("gulp-debug");
const less = require('gulp-less');
const browserify = require("browserify");
const babelify = require("babelify");
const source = require("vinyl-source-stream");
const fs = require('fs');
const path = require('path');

const ADMIN_PATH = 'modules/Adm/resources/assets';
const CSS_DEST_PATH = 'public/admin/css';
const LIBS_DEST_PATH = 'public/admin/js';
const MODULES_DEST_PATH = 'public/admin/js';

const THEME_BASE_PATH = 'source/metronic_theme/theme/classic/assets';
const THEME_DEST_PATH = 'public/admin';

gulp.task('admin:libs', function () {
  return gulp.src(`${ADMIN_PATH}/admin/libs/**/*`)
    .pipe(order([
      '/**/*',
      '/*',
    ]))
    .pipe(concat('libs.min.js'))
    .pipe(gulp.dest(LIBS_DEST_PATH));
});



gulp.task('admin:modules', function () {
  var b = browserify({
    extensions: [".jsx", ".js"],
    debug: false
  });

  const modulesFolder = path.resolve(__dirname, '../../../../../', 'modules');
  const modules = fs.readdirSync(modulesFolder, { withFileTypes: true })
    .filter(dirent => dirent.isDirectory())
    .map(dirent => dirent.name)
    // .filter(d => d !== 'Adm')
    .map((moduleName) => {
      if (fs.existsSync(path.resolve(modulesFolder, moduleName, 'resources/modules'))) {
        fs.readdirSync(path.resolve(modulesFolder, moduleName, 'resources/modules')).filter((file) => {
          return ['.js', '.jsx'].indexOf(path.extname(file)) > -1;
        }).forEach((file) => {
          b.add(path.resolve(modulesFolder, moduleName, 'resources/modules', file));
        });
      }

      if (fs.existsSync(path.resolve(modulesFolder, moduleName, 'resources/modules/components'))) {
        fs.readdirSync(path.resolve(modulesFolder, moduleName, 'resources/modules/components')).filter((file) => {
          return ['.js', '.jsx'].indexOf(path.extname(file)) > -1;
        }).forEach((file) => {
          b.add(path.resolve(modulesFolder, moduleName, 'resources/modules/components', file));
        });
      }
    });

  // fs.readdirSync(`${ADMIN_PATH}/admin/modules`).filter((file) => {
  //   return (file.slice(-3) === '.js');
  // }).forEach((file) => {
  //   b.add(`${ADMIN_PATH}/admin/modules/${file}`);
  // });

  // fs.readdirSync(path.resolve(modulesFolder, 'Adm/resources/modules/components')).filter((file) => {
  //   return (file.slice(-3) === '.js');
  // }).forEach((file) => {
  //   b.add(path.resolve(modulesFolder, 'Adm/resources/modules/components', file));
  // });

  return b.transform("babelify", {
    presets: [
      "es2015", "react", "stage-0", "stage-1", "stage-2", "stage-3"],
    plugins: [
      "syntax-decorators"
    ]
  })
    .bundle()
    .pipe(source('modules.min.js'))
    .pipe(gulp.dest(MODULES_DEST_PATH));
});

gulp.task('admin:theme:css:extends', function () {
  return gulp
    .src(`${ADMIN_PATH}/admin/css/**/**/**/**/*`)
    .pipe(concat('extends.min.css'))
    .pipe(cleanCSS({ compatibility: 'ie8' }))
    .pipe(gulp.dest(CSS_DEST_PATH));
});

gulp.task('admin:theme:css', gulp.series(
  'admin:theme:css:extends',
  // 'admin:theme:css:bundle'
), function () { });


if (args.production === undefined) {
  gulp.watch(`${ADMIN_PATH}/adm/**/*.js`, gulp.series('admin:modules'));
  gulp.watch(`${ADMIN_PATH}/admin/libs/**/**/**/*.js`, gulp.series('admin:libs'));
  gulp.watch(`${ADMIN_PATH}/admin/css/**/*`, gulp.series('admin:theme:css'));

  const modulesFolder = path.resolve(__dirname, '../../../../../', 'modules');
  const modules = fs.readdirSync(modulesFolder, { withFileTypes: true })
    .filter(dirent => dirent.isDirectory())
    .map(dirent => dirent.name)
    // .filter(d => d !== 'Adm')
    .forEach((moduleName) => {
      if (fs.existsSync(path.resolve(modulesFolder, moduleName, 'resources/modules'))) {
        gulp.watch(path.resolve(modulesFolder, moduleName, 'resources/modules/**/*.js'), gulp.series('admin:modules'));
        gulp.watch(path.resolve(modulesFolder, moduleName, 'resources/modules/**/*.jsx'), gulp.series('admin:modules'));
      }
    });
}

gulp.task('admin', gulp.series(
  'admin:theme:css',
  'admin:libs',
  'admin:modules'
));
