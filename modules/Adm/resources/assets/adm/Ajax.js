/*
    AJAX default settings
*/
const notification = require('./Notification');
const preloader = require('./Preloader');

$.ajaxSetup({
  statusCode: {
    401: () => {
      window.location.href = ADM.managerPath;
    },
    403: () => {
      notifications.error({ 
        message: "You are not have permissions for this action." 
      });
    },
    404: () => {
      notification.error({ 
        message: "Page not found." 
      });
    }
  },
  error: function () {
    notification.error({ 
      message: "Something was wrong." 
    });
  }
});

const Ajax = (params) => {
  let _success = params.success;
  delete params.success;

  //preloader.open();
  params = $.extend(true, {}, {
    type: 'post',
    dataType: 'json',
    cache: false,
    url: '',
    error: (request) => {
      //preloader.close();
      if (parseInt(request.status) == 401) {
        window.location.href = ADM.managerPath + '/account/login';
      } else {
        ADM.RequestParse(request);
        if (_success) {
          if (typeof _success === 'function') {
            _success(request);
          } else if (_success.indexOf('::')) {
            let _tmp = _success.split('::');
            _success = ADM.modules.get(_tmp[0])[_tmp[1]];
            _success(request, ADM.modules.get(_tmp[0]));
          }
        }
      }
    },
    success: (request, codeMessage, xhr) => {
      //preloader.close();
      ADM.RequestParse(request);
      if (_success) {
        if (typeof _success === 'function') {
          _success(request);
        } else if (_success.indexOf('::')) {
          let _tmp = _success.split('::');
          _success = ADM.modules.get(_tmp[0])[_tmp[1]];
          _success(request, ADM.modules.get(_tmp[0]));
        }
      }
    }
  }, params);
  $.ajax(params);
};
module.exports = Ajax;
