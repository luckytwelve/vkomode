if (typeof userId !== 'undefined') {
  window.socket = io.connect('/', { query: `userId=${userId}` });
  const audio = new Audio('/admin/media/notification.mp3');
  socket.on('message', (data) => {
    if (data.type === 'notify') {
      ADM.notification.success(data, { delay: 12000 });
      audio.play();
    }
  });
}


module.export = {};