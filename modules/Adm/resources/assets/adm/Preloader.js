class Preloader {

  open(){
    $('body').prepend($('<div class="main-preloader"><div class="preloader"></div></div>'))
  }
  close(){
    $('.main-preloader').remove();
  }
}

module.exports = new Preloader();