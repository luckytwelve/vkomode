/* eslint func-names: ["error", "never"] */
/* eslint prefer-arrow-callback: 0 */
/* global $,CKEDITOR, window */

const ADM = require('../../../../Adm/resources/assets/adm/adm');

ADM.ui.set('datetimepicker', {
  init: (context) => {
    $('.datetimepicker', context).datetimepicker({
      format: 'yyyy-mm-dd hh:ii',
      todayHighlight: true,
      bootcssVer: 3,
      autoclose: true,
    });
  }
});

ADM.ui.set('datepicker', {
  init: (context) => {
    $('.datepicker', context).datepicker({
      format: 'yyyy-mm-dd',
    });
  }
});
