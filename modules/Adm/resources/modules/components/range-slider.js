/* eslint func-names: ["error", "never"] */
/* eslint prefer-arrow-callback: 0 */
/* global $,CKEDITOR, window */

const ADM = require('../../../../Adm/resources/assets/adm/adm');

ADM.ui.set('kt_slider', {
  init: (context) => {
    $('.kt_slider', context).each(function(){
      $(this).ionRangeSlider()
    });
  }
});
