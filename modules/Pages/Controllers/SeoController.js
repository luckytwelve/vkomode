const Route = use('Route');
const View = use('View');
const Datatables = use('ADM/Datatables');
const TableBuilder = use('ADM/TableBuilder');
const Database = use('Database');
const { validate } = use('Validator');
const Seo = use('Pages/Models/Seo');
const Notify = use('ADM/Notify');


class SeoController {
  async index({ view, __, auth }) {
    const permissions = await auth.user.getPermissions();

    const table = new TableBuilder('seo');

    table.setName(__('Pages.pages.seo_list'));

    if (permissions.indexOf('seo_create') !== -1) {
      table.setButtons([
        [`<a href="${Route.url('Seo.seo.edit')}" class="pull-right btn btn-success">${__('Adm.admin.create')}</a>`]
      ]);
    }

    table.setColums([
      { title: '#', width: '1%' },
      { title: __('Pages.pages.page_url'), width: '5%' },
      { title: __('Pages.pages.seo_title'), width: '10%' },
      { title: __('Pages.pages.seo_description'), width: '10%' },
      { title: __('Pages.pages.seo_keywords'), width: '10%' },
      { title: __('Adm.admin.actions'), width: '1%' }
    ]);

    View.global('breadcrumbs', [
      { name: __('Adm.admin.home'), url: Route.url('admin.dashboard.index') },
      { name: __('Pages.pages.seo'), url: Route.url('Seo.seo.list') }
    ]);

    return view.render('Pages.seo.index', {
      table: table.build()
    });
  }

  async list({ request, response }) {
    const query = Database
      .select(
        'seo.id',
        'seo.slug',
        'seo.meta_title',
        'seo.meta_description',
        'seo.meta_keywords'
      )
      .from('seo');

    const table = new Datatables(query, request);
    const res = await table.make();
    return response.json(res);
  }

  async edit({ response, params, __ }) {
    const { id } = params;
    const data = await Seo.find(Number(id));

    if (id && !data) {
      return response.json(Notify.error('Not found', {}));
    }

    return response.json({
      modal: {
        title: id ? __('Pages.pages.seo_edit') : __('Pages.pages.create_new_seo'),
        content: View.render('Pages.seo.form', { data }),
        cancel: __('Adm.admin.cancel'),
        submit: __('Adm.admin.save')
      },
      success: true
    });
  }

  async save({ request, response }) {
    const input = request.all();

    const rules = {
      meta_title: 'required|string',
      meta_description: 'required|string',
      meta_keywords: 'required|string',
      slug: `required|string|unique:seo,slug,id,${input.id ? input.id : ''}`
    };

    const messages = {
      'meta_title.required': 'Поле название обязательное',
      'meta_description.required': 'Поле описане обязательное',
      'meta_keywords.required': 'Поле описане обязательное',
      'slug.required': 'Поле Псевдоним обязательное',
      'slug.unique': 'Страница с таким значением поля Псевдоним уже существует. Введите другое название'
    };

    const validation = await validate(input, rules, messages);
    if (validation.fails()) {
      return response.json(Notify.error(validation.messages()[0].message, {}));
    }

    let seo = {};

    if (!input.id) {
      seo = new Seo();
    } else {
      seo = await Seo.find(Number(input.id));
      if (!seo) {
        return response.json(Notify.error('Seo not found', {}))
      }
    }

    seo.meta_title = input.meta_title;
    seo.slug = input.slug;
    seo.meta_description = input.meta_description;
    seo.meta_keywords = input.meta_keywords;
    await seo.save();

    return response.json(Notify.success('Сохранено', {}));
  }


  async delete({ request, response, params }) {
    const { id } = params;
    const seo = await Seo.find(Number(id));

    if (!seo) {
      return response.json(Notify.error('Something went wrong. page not found', {}));
    }

    if (await seo.delete()) {
      return response.json(Notify.success('Deleted', {}));
    }
    return true;
  }
}

module.exports = SeoController;
