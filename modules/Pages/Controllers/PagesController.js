const Route = use('Route');
const View = use('View');
const Datatables = use('ADM/Datatables');
const TableBuilder = use('ADM/TableBuilder');
const Database = use('Database');
const { validate } = use('Validator');
const Page = use('Pages/Models/Page');
const Notify = use('ADM/Notify');
const SiteMap = use('Shop/Services/SiteMap');

class PagesController {
  async index({ view, __, auth }) {
    const permissions = await auth.user.getPermissions();

    const table = new TableBuilder('pages');

    table.setName(__('Pages.pages.list'));

    if (permissions.indexOf('pages_create') !== -1) {
      table.setButtons([
        [`<a href="${Route.url('Pages.pages.edit')}" class="pull-right btn btn-success">${__('Adm.admin.create')}</a>`]
      ]);
    }

    table.setColums([
      { title: '#', width: '1%' },
      { title: __('Pages.pages.slug'), width: '10%' },
      { title: __('Pages.pages.title'), width: '10%' },
      { title: __('Pages.pages.active'), width: '5%' },
      { title: __('Adm.admin.actions'), width: '1%' }
    ]);

    View.global('breadcrumbs', [
      { name: __('Adm.admin.home'), url: Route.url('admin.dashboard.index') },
      { name: __('Pages.pages.pages'), url: Route.url('Pages.pages.list') }
    ]);

    return view.render('Pages.pages.index', {
      table: table.build()
    });
  }

  async list({ request, response }) {
    const query = Database
      .select(
        'pages.id',
        'pages.slug',
        'pages.title',
        'pages.active'
      )
      .from('pages');

    const table = new Datatables(query, request);
    const res = await table.make();
    return response.json(res);
  }

  async edit({ response, params, __ }) {
    const { id } = params;
    const data = await Page.find(Number(id));

    if (id && !data) {
      return response.json(Notify.error('Not found', {}));
    }

    return response.json({
      modal: {
        title: id ? __('Pages.pages.edit') : __('Pages.pages.create_new'),
        content: View.render('Pages.pages.form', { data }),
        cancel: __('Adm.admin.cancel'),
        submit: __('Adm.admin.save')
      },
      success: true
    });
  }

  async save({ request, response }) {
    const input = request.all();

    const rules = {
      title: 'required|string',
      content: 'required|string',
      slug: `required|string|unique:pages,slug,id,${input.id ? input.id : ''}`
    };

    const messages = {
      'title.required': 'Поле название обязательное',
      'content.required': 'Поле описане обязательное',
      'slug.required': 'Поле Псевдоним обязательное',
      'slug.unique': 'Страница с таким значением поля Псевдоним уже существует. Введите другое название'
    };

    const validation = await validate(input, rules, messages);
    if (validation.fails()) {
      return response.json(Notify.error(validation.messages()[0].message, {}));
    }

    let page = {};

    if (!input.id) {
      page = new Page();
    } else {
      page = await Page.find(Number(input.id));
      if (!page) {
        return response.json(Notify.error('Page not found', {}))
      }
    }

    page.title = input.title;
    page.slug = input.slug;
    page.description = input.content;
    page.active = !!input.active;
    await page.save();
    await SiteMap.generate();

    return response.json(Notify.success('Сохранено', {}));
  }


  async delete({ request, response, params }) {
    const { id } = params;
    const page = await Page.find(Number(id));

    if (!page) {
      return response.json(Notify.error('Something went wrong. page not found', {}));
    }

    if (await page.delete()) {
      return response.json(Notify.success('Deleted', {}));
    }
    return true;
  }
}

module.exports = PagesController;
