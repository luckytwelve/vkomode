const Route = use('Route');

Route.group(() => {
  
 // Pages
 Route
   .any('pages', '@provider:Pages/Controllers/PagesController.index')
   .as('Pages.pages.list')
   .middleware(['managerXHR', 'managerAuth', 'can:pages_view']);
 
 Route
   .post('pages/list', '@provider:Pages/Controllers/PagesController.list')
   .as('Pages.pages.list')
   .middleware(['managerXHR', 'managerAuth', 'can:pages_view']);
 
 Route
   .post('pages/edit/:id?', '@provider:Pages/Controllers/PagesController.edit')
   .as('Pages.pages.edit')
   .middleware(['managerXHR', 'managerAuth', 'can:pages_edit']);
 
 Route
   .post('pages/delete/:id', '@provider:Pages/Controllers/PagesController.delete')
   .as('Pages.pages.delete')
   .middleware(['managerXHR', 'managerAuth', 'can:pages_delete']);
 
 Route
   .post('pages/save', '@provider:Pages/Controllers/PagesController.save')
   .as('Pages.pages.save')
   .middleware(['managerXHR', 'managerAuth', 'can:pages_edit']);

 // Seo
 Route
   .any('seo', '@provider:Pages/Controllers/SeoController.index')
   .as('Seo.seo.list')
   .middleware(['managerXHR', 'managerAuth', 'can:seo_view']);

 Route
   .post('seo/list', '@provider:Pages/Controllers/SeoController.list')
   .as('Seo.seo.list')
   .middleware(['managerXHR', 'managerAuth', 'can:seo_view']);

 Route
   .post('seo/edit/:id?', '@provider:Pages/Controllers/SeoController.edit')
   .as('Seo.seo.edit')
   .middleware(['managerXHR', 'managerAuth', 'can:seo_edit']);

 Route
   .post('seo/delete/:id', '@provider:Pages/Controllers/SeoController.delete')
   .as('Seo.seo.delete')
   .middleware(['managerXHR', 'managerAuth', 'can:seo_delete']);

 Route
   .post('seo/save', '@provider:Pages/Controllers/SeoController.save')
   .as('Seo.seo.save')
   .middleware(['managerXHR', 'managerAuth', 'can:seo_edit']);
}).prefix('admin');
