'use strict'

module.exports = {

  'Pages':
    [
      {
        'slug': 'pages_view',
        'name': 'Pages view'
      },
      {
        'slug': 'pages_create',
        'name': 'Pages create'
      },
      {
        'slug': 'pages_edit',
        'name': 'Pages edit'
      },
      {
        'slug': 'pages_delete',
        'name': 'Pages delete'
      }
    ],
  'Seo':
    [
      {
        'slug': 'seo_view',
        'name': 'Seo view'
      },
      {
        'slug': 'seo_create',
        'name': 'Seo create'
      },
      {
        'slug': 'seo_edit',
        'name': 'Seo edit'
      },
      {
        'slug': 'seo_delete',
        'name': 'Seo delete'
      }
    ],
};
