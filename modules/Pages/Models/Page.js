'use strict'

const Model = use('Model');

class Page extends Model {
  static get table() {
    return 'pages';
  }
}

module.exports = Page;
