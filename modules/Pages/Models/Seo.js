'use strict'

const Model = use('Model');

class Seo extends Model {
  static get table() {
    return 'seo';
  }
}

module.exports = Seo;