/* global permissions, $, __, window */

const ADM = require('../../../Adm/resources/assets/adm/adm');

ADM.modules.set('pages', {
  tableSelector: '#pages',

  ReloadTable(response, module) {
    $(module.tableSelector).DataTable().ajax.reload(null, false);
  },

  AfterFormSend(response, module) {
    if (!response.notification) return;
    if (response.notification.type === 'success') {
      $('.modal').modal('hide');
      $(module.tableSelector).DataTable().ajax.reload(null, false);
    }
  },

  init(...args) {
    this.table = $(this.tableSelector).DataTable({
      scrollX: true,
      scrollCollapse: true,
      processing: true,
      serverSide: true,

      initComplete: (oSettings) => {
        $(window).resize();
      },

      drawCallback: (oSettings) => {
        $(window).trigger('resize');
      },

      ajax: {
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: `${ADM.managerPath}/pages/list`,
        type: 'POST',
        dataType: 'json',
      },

      columns: [
        { data: 'id', name: 'pages.id' },
        { data: 'slug', name: 'pages.slug' },
        {
          data: 'title',
          name: 'pages.title',
          orderable: true,
          searchable: true
        },
        {
          data: 'active',
          name: 'pages.active',
          orderable: true,
          searchable: true,
          render: (data, type, row, meta) => {
            return data ? 'Да' : 'Нет'
          }
        },

        {
          data: 'actions',
          name: 'actions',
          orderable: false,
          searchable: false,
          render: (data, type, row, meta) => {
            const actions = [];
            if (permissions.indexOf('pages_edit')) {
              actions.push(`
                <li class="nav-item">
                  <a class="nav-link no-history" href="${ADM.managerPath}/pages/edit/${row.id}" data-callback="pages::ReloadTable">
                    <i class="nav-icon la la-edit"></i>
                    <span class="nav-text">${__('Adm.admin.edit')}</span>
                  </a>
                </li>
              `);
            }
            actions.push(`
             <li class="nav-item">
              <a class="nav-link no-history no-ajax" href="${row.slug}" target="_blank">
                <i class="nav-icon la la-file-code"></i>
                <span class="nav-text">${__('Adm.admin.view')}</span>
              </a>
             </li>
              `);

            if (permissions.indexOf('pages_delete')) {
              actions.push(`
                <li class="nav-item">
                  <a class="nav-link no-history" href="${ADM.managerPath}/pages/delete/${row.id}" data-callback="pages::ReloadTable" data-confirm="Are you sure you want to delete item with ID ${row.id}?">
                    <i class="nav-icon la la-trash"></i>
                    <span class="nav-text">${__('Adm.admin.remove')}</span>
                  </a>
                </li>
              `);
            }

            if (!actions.length) return '---';

            return `
            <div class="dropdown dropdown-inline">
              <a href="javascript:;" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown" aria-expanded="false">
                <i class="la la-cog"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" style="display: none;">
                <ul class="nav nav-hoverable flex-column">
                  ${actions.join('')}
                </ul>
              </div>
            </div>
            `;
          }
        }
      ],
    });
  }
});
