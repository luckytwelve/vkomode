/* global permissions, $, __, window */

const ADM = require('../../../Adm/resources/assets/adm/adm');

ADM.modules.set('seo', {
  tableSelector: '#seo',

  ReloadTable(response, module) {
    $(module.tableSelector).DataTable().ajax.reload(null, false);
  },

  AfterFormSend(response, module) {
    if (!response.notification) return;
    if (response.notification.type === 'success') {
      $('.modal').modal('hide');
      $(module.tableSelector).DataTable().ajax.reload(null, false);
    }
  },

  init(...args) {
    this.table = $(this.tableSelector).DataTable({
      scrollX: true,
      scrollCollapse: true,
      processing: true,
      serverSide: true,

      initComplete: (oSettings) => {
        $(window).resize();
      },

      drawCallback: (oSettings) => {
        $(window).trigger('resize');
      },

      ajax: {
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: `${ADM.managerPath}/seo/list`,
        type: 'POST',
        dataType: 'json',
      },

      columns: [
        { data: 'id', name: 'seo.id' },
        { data: 'slug', name: 'seo.slug' },
        {
          data: 'meta_title',
          name: 'seo.meta_title',
          orderable: true,
          searchable: true
        },
        {
          data: 'meta_description',
          name: 'seo.meta_description',
          orderable: true,
          searchable: true
        },
        {
          data: 'meta_keywords',
          name: 'seo.meta_keywords',
          orderable: true,
          searchable: true
        },
        {
          data: 'actions',
          name: 'actions',
          orderable: false,
          searchable: false,
          render: (data, type, row, meta) => {
            const actions = [];
            if (permissions.indexOf('seo_edit')) {
              actions.push(`
                <li class="nav-item">
                  <a class="nav-link no-history" href="${ADM.managerPath}/seo/edit/${row.id}" data-callback="seo::ReloadTable">
                    <i class="nav-icon la la-edit"></i>
                    <span class="nav-text">${__('Adm.admin.edit')}</span>
                  </a>
                </li>
              `);
            }

            if (permissions.indexOf('seo_delete')) {
              actions.push(`
                <li class="nav-item">
                  <a class="nav-link no-history" href="${ADM.managerPath}/seo/delete/${row.id}" data-callback="seo::ReloadTable" data-confirm="Are you sure you want to delete item with ID ${row.id}?">
                    <i class="nav-icon la la-trash"></i>
                    <span class="nav-text">${__('Adm.admin.remove')}</span>
                  </a>
                </li>
              `);
            }

            if (!actions.length) return '---';

            return `
            <div class="dropdown dropdown-inline">
              <a href="javascript:;" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown" aria-expanded="false">
                <i class="la la-cog"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" style="display: none;">
                <ul class="nav nav-hoverable flex-column">
                  ${actions.join('')}
                </ul>
              </div>
            </div>
            `;
          }
        }
      ],
    });
  }
});
