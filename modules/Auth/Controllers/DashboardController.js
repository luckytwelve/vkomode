'use strict';

const Database = use('Database');

class DashboardController {
  async index({ view, auth }) {
    return view.render('Auth.dashboard.index', {});
  }
}

module.exports = DashboardController;
