'use strict';
const { validate } = use('Validator');
const Notify = use('ADM/Notify');

class AccountController {
  index({ view }) {
    return view.render('Auth.account.login');
  }

  async login({ request, response, auth }) {
    
    const { email, password } = request.all();
    const rules = {
      email: 'required|min:3|max:255',
      password: 'required|min:6|max:32'
    };

    const validation = await validate(request.all(), rules);
    if (validation.fails()) {
      return response.json(Notify.error(validation.messages()[0].message));
    }

    
    try {
      const user = await auth.authenticator('manager')
        .remember(true)
        .attempt(email, password);

      if (user.blocked === 1) {
        await auth.authenticator('manager').logout();
        console.log('User blocked')
        return response.json(Notify.error('User blocked', {}));
      }
      console.log('Validation success')
      return response.json(Notify.success('Вход выполнен успешно', {}));
    } catch (e) {
      console.log(e);
      return response.json(Notify.error('Manager not found', {}));
    }
  }

  async logout({ response, auth }) {
    await auth.authenticator('manager')
      .logout();
    return { success: true };
  }
}

module.exports = AccountController;
