const ADM = require('../../../Adm/resources/assets/adm/adm');

ADM.modules.set('login', {
  ADMcontroller: 'login',
  events: {
    'submit: #loginForm': 'login',
  },

  init() {

    $("#loginForm").validate({
      rules: {
        email: {
          required: true,
          email: true,
          minlength: 9
        },
        password: {
          required: true,
          minlength: 6
        }
      },
      submitHandler: (form) => {
        this.login(form)
      }
    });

  },

  login: function (form) {
    $('button[type=submit]', form).prop('disabled', true);

    ADM.ajax({
      type: 'post',
      dataType: 'json',
      cache: false,
      url: ADM.managerPath + '/auth/login',
      data: $(form).serialize(),
      success: (response) => {
        $('button[type=submit]', form).prop('disabled', false);
        switch (response.notification.type) {
          case 'success':
            setTimeout(document.location.href = ADM.managerPath, 1000);
            break;
        }
      },
      error: () => {
        $('button[type=submit]', form).prop('disabled', false);
      }
    });

    return false;
  }
});
