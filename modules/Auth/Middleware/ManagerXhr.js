'use strict'

const View = use('View')
const Config = use('Config')

class ManagerXhr {
  async handle({ request, response, auth, view }, next) {
    if (!request.ajax()) {
      try {
        // await auth.loginIfCan();
        // await auth.authenticator('manager').check();
        const user = await auth.authenticator('manager').getUser();
        const passed = (await user.getRoles()).filter(r => ['manager', 'admin'].indexOf(r) > -1).length
        if ((user.id !== 1 && !passed)) {
          throw "not a manager";
        }
        if (user.blocked === 1) {
          await auth.authenticator('manager').logout();
          return response.redirect('/admin');
        }

        View.global('user', {
          ...(user.toJSON()),
          roles: await user.getRoles(),
          permissions: await user.getPermissions(),
          permissions_json: JSON.stringify(await user.getPermissions())
        });

        // view.share('user', {
        //   ...(user.toJSON()),
        //   roles: await user.getRoles(),
        //   permissions: await user.getPermissions(),
        //   permissions_json: JSON.stringify(await user.getPermissions())
        // });

        return response.send(
          view.render(Config.get('admin.general.masterTemplate', 'Adm.index.index'), {})
        );
      } catch (e) {
        console.log(e);
      }
    }
    await next()
  }
}

module.exports = ManagerXhr;
