
class ManagerAuth {
  async handle({
    request,
    response,
    auth,
    view
  }, next) {
    try {
      const user = await auth.authenticator('manager').getUser();
      const passed = (await user.getRoles()).filter(r => ['manager', 'admin'].indexOf(r) > -1).length;

      auth.user = user;

      if (user.blocked === 1) {
        await auth.authenticator('manager').logout();
        return response.unauthorized('Login');
      }

      if ((user.id !== 1 && !passed)) {
        await auth.authenticator('manager').logout();
        throw new Error('not a manager');
      }

      view.share({ user: user.toJSON() });
    } catch (e) {
      console.log(e)
      if (request.ajax()) {
        return response.unauthorized('Login');
      }
      return response.redirect('/admin', true);
    }

    return await next();
  }
}

module.exports = ManagerAuth;
