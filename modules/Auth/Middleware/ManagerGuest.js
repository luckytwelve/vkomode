require('dotenv').config();

class ManagerGuest {
  async handle({ request, response, auth }, next) {
    try {
      const user = await auth.authenticator('manager').getUser();
      const passed = (await user.getRoles()).filter(r => ['manager', 'admin'].indexOf(r) > -1).length;
      if (user.blocked === 1) {
        await auth.authenticator('manager').logout();
      }

      if (user.id !== 1 && !passed) {
        await auth.authenticator('manager').logout();
        throw "not a manager (Guest)";
      }
      return response.redirect(`/admin/${process.env.ADMIN_HOME_PAGE || 'default-dashboard'}`);
    } catch (e) {
      console.log(e);
    }

    return await next();
  }
}

module.exports = ManagerGuest;
