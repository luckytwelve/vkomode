const Route = use('Route');

Route.group(() => {
  //Auth
  Route
    .get('/', '@provider:Auth/Controllers/AccountController.index')
    .as('admin.loginPage')
    .middleware(['managerGuest']);

  Route
    .post('auth/login', '@provider:Auth/Controllers/AccountController.login')
    .as('admin.login')
    .middleware(['managerGuest']);

  Route
    .any('auth/logout', '@provider:Auth/Controllers/AccountController.logout')
    .as('admin.logout');

  Route
    .any('default-dashboard', '@provider:Auth/Controllers/DashboardController.index')
    .as('admin.dashboard')
    .middleware(['managerXHR', 'managerAuth']);

}).prefix('admin');
