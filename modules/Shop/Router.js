const Route = use('Route');

// Route.on('/test').render('Shop.test.test3');
// Route.get('/test2', '@provider:Shop/Controllers/ProductController.test');

// products
Route.group(() => {
  Route
    .any('products', '@provider:Shop/Controllers/ProductsController.index')
    .as('admin.products.list')
    .middleware(['managerXHR', 'managerAuth', 'can:products_view']);

  Route
    .post('products/list', '@provider:Shop/Controllers/ProductsController.list')
    .as('admin.products.list')
    .middleware(['managerXHR', 'managerAuth', 'can:products_view']);

  Route
    .post('products/edit/:id?', '@provider:Shop/Controllers/ProductsController.edit')
    .as('admin.products.edit')
    .middleware(['managerXHR', 'managerAuth', 'can:products_edit']);

  Route
    .post('products/edit/:id/:action', '@provider:Shop/Controllers/ProductsController.edit')
    .as('admin.products.edit')
    .middleware(['managerXHR', 'managerAuth', 'can:products_edit']);

  Route
    .post('products/delete/:id', '@provider:Shop/Controllers/ProductsController.delete')
    .as('admin.products.delete')
    .middleware(['managerXHR', 'managerAuth', 'can:products_delete']);

  Route
    .post('products/save', '@provider:Shop/Controllers/ProductsController.save')
    .as('admin.products.save')
    .middleware(['managerXHR', 'managerAuth', 'can:products_edit']);

  // categories
  Route
    .any('categories', '@provider:Shop/Controllers/CategoriesController.index')
    .as('admin.categories.list')
    .middleware(['managerXHR', 'managerAuth', 'can:categories_view']);

  Route
    .post('categories/list', '@provider:Shop/Controllers/CategoriesController.list')
    .as('admin.categories.list')
    .middleware(['managerXHR', 'managerAuth', 'can:categories_view']);

  Route
    .post('categories/edit/:id?', '@provider:Shop/Controllers/CategoriesController.edit')
    .as('admin.categories.edit')
    .middleware(['managerXHR', 'managerAuth', 'can:categories_edit']);

  Route
    .post('categories/delete/:id', '@provider:Shop/Controllers/CategoriesController.delete')
    .as('admin.categories.delete')
    .middleware(['managerXHR', 'managerAuth', 'can:categories_delete']);

  Route
    .post('categories/save', '@provider:Shop/Controllers/CategoriesController.save')
    .as('admin.categories.save')
    .middleware(['managerXHR', 'managerAuth', 'can:categories_edit']);

}).prefix('admin');
