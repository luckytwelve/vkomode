'use strict'

const moment = require('moment');

const Route = use('Route');
const View = use('View');
const Datatables = use('ADM/Datatables');
const TableBuilder = use('ADM/TableBuilder');
const Database = use('Database');
const { validate } = use('Validator');
const Category = use('Shop/Models/Category');
const Notify = use('ADM/Notify');
const Helpers = use('Helpers');

class CategoryController {
  async index({ view, __, auth }) {
    const permissions = await auth.user.getPermissions();

    const table = new TableBuilder('categories');

    table.setName(__('Shop.categories.list'));

    if (permissions.indexOf('categories_create') !== -1) {
      table.setButtons([[`<a href="${Route.url('admin.categories.edit')}" class="pull-right btn btn-success">${__('Adm.admin.create')}</a>`]]);
    }

    table.setColums([
      { title: '#', width: '1%' },
      { title: __('Shop.categories.title'), width: '' },
      { title: __('Shop.categories.parent'), width: '' },
      { title: __('Adm.admin.actions'), width: '1%' }
    ]);

    View.global('breadcrumbs', [
      { name: __('admin.home'), url: Route.url('admin.dashboard.index') },
      { name: __('Shop.categories.categories'), url: Route.url('Shop.categories.list') }
    ]);

    return view.render('Shop.categories.index', {
      table: table.build()
    });
  }

  async list({ request, response }) {
    const query = Database
      .select(
        'categories.id',
        'categories.title',
        'categories.parent_id'
      )
      .from('categories');

    const table = new Datatables(query, request);
    const res = await table.make();

    const parents = (await Category.query().fetch()).toJSON();

    res.data = res.data.map((item) => {
      const parent = parents.find(p => Number(p.id) === Number(item.parent_id));
      return {
        ...item,
        parent: parent
          ? `<span class="label label-lg label-light-success label-inline">${parent.title} (${parent.id})</span>`
          : '<span class="label label-lg label-light-primary label-inline">Главная (0)</span>'
      };
    });
    return response.json(res);
  }

  async edit({ response, params, __ }) {
    const { id } = params;
    let data = {};

    const categories = (await Category.all()).toJSON();

    if (id) {
      data = await Category.find(Number(id));
      if (!id) {
        return response.json(Notify.error('Not found', {}));
      }
    }
    return response.json({
      modal: {
        title: id ? __('Shop.categories.edit') : __('Shop.categories.create_new'),
        content: View.render('Shop.categories.form', {
          fields: data,
          categories
        }),
        cancel: __('Adm.admin.cancel'),
        submit: __('Adm.admin.save')
      },
      success: true
    });
  }

  async save({ request, response }) {
    const input = request.all();

    // const rules = {
    //   login: `min:3|max:64|unique:users,login,id,${isEdit ? input.id : ''}`,
    //   email: `required|email|unique:users,email,id,${isEdit ? input.id : ''}`,
    //   first_name: 'required|min:3|max:255',
    //   last_name: 'required|min:3|max:255',
    //   password: `${isEdit ? '' : 'required|'}min:6|max:64|same:repassword`
    // }

    // const validation = await validate(input, rules);
    // if (validation.fails()) {
    //   return response.json(Notify.error(validation.messages()[0].message, {}))
    // }

    let category = {};

    if (!input.id) {
      category = new Category();
    } else {
      category = await Category.find(Number(input.id));
      if (!category) {
        return response.json(Notify.error('Manager not found', {}))
      }
    }

    category.order = Number(input.order);
    category.slug = input.slug;
    category.title = input.title;
    category.description = input.content;
    category.active = !!input.active;
    category.parent_id = Number(input.parent_id);
    await category.save();

    return response.json(Notify.success('Сохранено', {}))
  }




  async delete({ request, response, params }) {
    const { id } = params;
    const category = await Category.find(Number(id));

    if (!category) {
      return response.json(Notify.error('Something went wrong. category not found', {}));
    }

    if (await category.delete()) {
      return response.json(Notify.success('Deleted', {}));
    }
  }
}

module.exports = CategoryController;
