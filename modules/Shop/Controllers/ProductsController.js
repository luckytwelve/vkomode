'use strict';

const Route = use('Route');
const View = use('View');
const Database = use('Database');

const Datatables = use('ADM/Datatables');
const TableBuilder = use('ADM/TableBuilder');
const Notify = use('ADM/Notify');
const Product = use('Shop/Models/Product');
const Category = use('Shop/Models/Category');
const CategoryProduct = use('Shop/Models/CategoryProduct');
const Image = use('App/Models/Image');
const ProductMod = use('Shop/Models/ProductMod');
const { validate } = use('Validator');
const SiteMap = use('Shop/Services/SiteMap');

class ProductsController {
  async index({ view, __, auth }) {
    const permissions = await auth.user.getPermissions();
    const table = new TableBuilder('products');

    table.setName(__('Shop.products.list'));

    if (permissions.indexOf('products_create') !== -1) {
      table.setButtons([[`<a href="${Route.url('admin.products.edit')}" class="btn btn-success">${__('Adm.admin.create')}</a>`]]);
    }

    table.setColums([
      { title: '#', width: '1%' },
      { title: __('Shop.products.published'), width: '10%' },
      { title: __('Shop.products.name'), width: '15%' },
      { title: __('Shop.products.price_base'), width: '10%' },
      { title: __('Shop.products.price_local'), width: '10%' },
      { title: __('Shop.products.image'), width: '10%' },
      { title: __('Adm.admin.actions'), width: '1%' }
    ]);

    View.global('breadcrumbs', [
      { name: __('Adm.admin.home'), url: Route.url('admin.dashboard.index') },
      { name: __('Shop.products.products'), url: Route.url('Shop.products.list') }
    ]);

    return view.render('Shop.products.index', {
      table: table.build()
    });
  }

  async list({ request, response }) {
    const query = Database
      .select(
        'products.id',
        'products.active',
        'products.slug',
        'products.title',
        'products.art',
        'products.price_base',
        'products.price_local'
      )
      .from('products');

    const table = new Datatables(query, request);
    const res = await table.make();

    const ids = res.data.map(d => d.id);
    const images = (await Image.query()
      .where('entity', 'product')
      .whereIn('entity_id', ids)
      .fetch()).toJSON();

    res.data = res.data.map((d) => {
      const image = images.find(i => i.entity_id === d.id);

      return {
        ...d,
        image: image ? image.path : null
      };
    });

    return response.json(res);
  }


  /** edit
   *
   */
  async edit({ response, params, __ }) {
    const { id, action } = params;
    let data = {};
    let categoriesSelected = [];

    if (id) {
      data = (await Product.find(Number(id))).toJSON();
      if (!id) {
        return response.json(Notify.error('Not found', {}));
      }

      categoriesSelected = (await CategoryProduct.query().where({
        product_id: id
      }).fetch()).rows;

      const productMod = (await ProductMod.query()
        .where('product_id', Number(id))
        .fetch()
      ).toJSON();

      const image = await Image.query()
        .where('entity', 'product')
        .where('entity_id', Number(id))
        .first();

      data.image = image ? image.path : '';

      const images = (await Image.query()
        .where('entity', 'product_mod')
        .whereIn('entity_id', productMod.map(p => p.id))
        .fetch()
      ).toJSON();

      data.mods = productMod.map((p) => {
        const img = images.find(i => i.entity_id === p.id);

        return {
          sku: p.sku,
          color: p.data ? p.data.color : '',
          image: img ? img.path : '',
          available: p.available
        };
      });
    }

    const categories = (await Category.query().orderBy('parent_id', 'ASC').fetch()).toJSON();

    const tree = (list) => {
      const map = {};
      let node = null;
      const roots = [];

      for (let i = 0; i < list.length; i += 1) {
        map[list[i].id] = i;
        list[i].children = [];
      }

      for (let i = 0; i < list.length; i += 1) {
        node = list[i];
        if (Number(node.parent_id) !== 0) {
          list[map[node.parent_id]].hasChildren = true;
          node.selected = categoriesSelected.find(c => c.category_id === node.id) ? true : false;
          node.parentTitle = list[map[node.parent_id]].title;
          list[map[node.parent_id]].children.push(node);
        } else {
          roots.push(node);
        }
      }
      return roots;
    };


    if (action === 'duplicate') {
      delete data.id;
    }

    return response.json({
      modal: {
        title: id ? __('Shop.products.edit') : __('Shop.products.create_new'),
        content: View.render('Shop.products.form', {
          categoriesTree: tree(categories),
          categoriesSelected,
          fields: data
        }),
        cancel: __('Adm.admin.cancel'),
        submit: __('Adm.admin.save')
      },
      success: true
    });
  }

  async save({ request, response }) {
    const input = request.all();
    let attributes = [];

    const rules = {
      title: 'required|string',
      content: 'required|string',
      slug: `required|string|unique:products,slug,id,${input.id ? input.id : ''}`,
      categories: 'required',
      price_base: 'required|number',
      price_local: 'required|number',
      art: 'required|string',
      quantity: 'required|number',
      image: 'required'
    };

    const messages = {
      'title.required': 'Поле название обязательное',
      'content.required': 'Поле описане обязательное',
      'slug.required': 'Поле Псевдоним обязательное',
      'slug.unique': 'Товар с таким значением поля Псевдоним уже существует. Введите другое название',
      'price_base.required': 'Поле Цена USD $ обязательное',
      'price_base.numeric': 'Поле Цена USD должно быть числовым',
      'price_local.required': 'Поле Цена ГРН обязательное',
      'price_local.numeric': 'Поле Цена ГРН должно быть числовым',
      'art.required': 'Поле артикул обязательное',
      'quantity.required': 'Поле количество обязательное',
      'quantity.numeric': 'Поле количество должно быть числовым',
      'image.required': 'Поле Главное изображение обязательное',
      'categories.required': 'Поле Категория обязательное'
    };

    const validation = await validate(input, rules, messages);
    if (validation.fails()) {
      return response.json(Notify.error(validation.messages()[0].message, {}));
    }
    input.attribute_name = input.attribute_name.filter(atn=>atn.length)
    input.attribute_value = input.attribute_value.filter(atv=>atv.length)
    if(input.attribute_name.length !== input.attribute_value.length){
      return response.json(Notify.error('Пустое имя характеристки или значения'));
    }

    if(input.attribute_name.length && input.attribute_value.length){
      for (let i = 0; i < input.attribute_name.length; i += 1) {
        attributes.push({
          name: input.attribute_name[i],
          value: input.attribute_value[i]
        })
      }
    }

    let product = {};

    if (!input.id) {
      product = new Product();
    } else {
      product = await Product.find(Number(input.id));
      if (!product) {
        return response.json(Notify.error('Продукт не найден', {}));
      }

      const mods = (await ProductMod.query().where('product_id', product.id).fetch()).toJSON();

      await Image.query()
        .where('entity', 'product_mod')
        .whereIn('entity_id', mods.map(m => m.id))
        .delete();

      await Image.query()
        .where('entity', 'product')
        .where('entity_id', Number(input.id))
        .delete();

      await ProductMod.query()
        .where('product_id', product.id)
        .delete();
    }

    let slug = input.slug ? input.slug.toLowerCase() : '';
    slug = slug
      .replace(/\s\s+/g, '_')
      .replace(/[^0-9a-zA-Z\-\_]/gi, '');

    product.active = !!input.active;
    product.special_offer = !!input.special_offer;
    product.best_product = !!input.best_product;
    product.title = input.title;
    product.description = input.content;
    product.slug = slug;
    product.price_base = Number(input.price_base);
    product.price_local = Number(input.price_local);
    product.art = input.art;
    product.quantity = input.quantity;
    product.attributes = JSON.stringify(attributes)
    product.meta = JSON.stringify(input.meta)
    await product.save();

    if (input.image) {
      await Image.create({
        entity: 'product',
        entity_id: product.id,
        path: input.image,
      });
    }

    for (let i = 0; i < input.mod_image.length; i += 1) {
      if (!input.mod_image[i]) {
        continue;
      }

      const modModel = await ProductMod.create({
        product_id: product.id,
        sku: product.art,//input.mod_sku[i],
        available: 1,//input.mod_available[i],
        data: {
          color: ''//input.mod_color[i]
        }
      });

      await Image.create({
        path: input.mod_image[i],
        entity: 'product_mod',
        entity_id: modModel.id,
      });
    }

    const categories = Array.isArray(input.categories) ? input.categories : [input.categories];

    await CategoryProduct.query().where({ product_id: product.id }).delete();
    await CategoryProduct.createMany(categories.map((c) => {
      return {
        product_id: Number(product.id),
        category_id: Number(c)
      };
    }));
    await SiteMap.generate();
    return response.json(Notify.success('Сохранено', {}));
  }

  async delete({ request, response, params }) {
    const { id } = params;
    const product = await Product.find(Number(id));

    if (!product) {
      return response.json(Notify.error('Something went wrong. Product not found', {}));
    }

    await Image.query().where({
      entity: 'product',
      entity_id: product.id
    }).delete();

    await product.delete();

    return response.json(Notify.success('Удалено', {}));
  }
}

module.exports = ProductsController;
