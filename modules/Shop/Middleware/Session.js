'use strict'

class Session {

  async handle({ view, params, request, locale, session }, next) {


    view.share({
      session: {
        ...session.all(),
        products: session.get('products', []).map((product)=>{
          return Object.entries(product)[0][1]
        }),
        total: session.get('total', 0.00),
        quantity: session.get('quantity', 0)
      }
    })

    await next();
  }
}

module.exports = Session;
