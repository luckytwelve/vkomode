const Product = use('Shop/Models/Product');
const Image = use('App/Models/Image');
const ProductMod = use('Shop/Models/ProductMod');

class ProductClass {
  async getByAlias(slug) {
    const query = Product.query().where('slug', slug);
    const product = await this.getProduct(query);
    return product;
  }

  async get(idx) {
    let result = null;

    if (Array.isArray(idx)) {
      result = Promise.all(idx.map(async (id) => {
        const product = await this.getProduct(id);
        return product;
      }));
    } else {
      const query = Product.query().where('id', idx);
      result = this.getProduct(query);
    }

    return result;
  }

  async getByIds(ids = []) {
    return Promise.all(ids.map(async (id) => {
      const product = await this.getProduct(id);
      return product;
    }));
  }

  async getProduct(query) {
    const product = (
      await query
        .with('categories')
        .fetch()
    ).toJSON()[0];

    // product.tags = (await Tag
    //   .query()
    //   .where({
    //     entity_id: product.id,
    //     entity: 'product'
    //   }).fetch()).toJSON();

    product.image = (await Image
      .query()
      .where({
        entity_id: product.id,
        entity: 'product'
      }).fetch()).toJSON();

    const productMod = (await ProductMod.query()
      .where('product_id', product.id)
      .orderBy('id', 'asc')
      .fetch()
    ).toJSON();

    const images = (await Image.query()
      .where('entity', 'product_mod')
      .whereIn('entity_id', productMod.map(p => p.id))
      .fetch()
    ).toJSON();

    product.mods = productMod.map((p) => {
      const img = images.find(i => i.entity_id === p.id);
      return {
        sku: p.sku,
        color: p.data ? p.data.color : '',
        image: img ? img.path : '',
      };
    });

    product.colors = productMod
      .map(p => p.sku)
      .filter((v, i, self) => self.indexOf(v) === i)
      .map(s => (productMod.find(p => p.sku === s)));

    return product;
  }


  processDiscount(product) {
    let discount = 0;

    if (product.sale) {
      discount = (
        (Number(product.price1) - Number(product.sale)) / (Number(product.price1) / 100)
      ).toFixed(0);
    }

    return discount;
  }
}

module.exports = new ProductClass();
