const { createWriteStream } = require('fs');
const { SitemapStream } = require('sitemap');
const Env = use('Env');
const Database = use('Database');
const Helpers = use('Helpers')

class SiteMap {
  async generate(){

    const sitemap = new SitemapStream({ hostname: Env.get('DOMAIN_NAME')});
    const writeStream = createWriteStream(Helpers.publicPath('sitemap.xml'));
    sitemap.pipe(writeStream);

    let urls = [];

    urls = urls.concat(await this.system())
    urls = urls.concat(await this.pages())
    urls = urls.concat(await this.products())

    for(let i=0; i < urls.length; i++){
      if(urls[i].img_url !== undefined){
        sitemap.write({
          url: urls[i].slug,
          img: urls[i].img_url
        });
      } else {
        sitemap.write(urls[i]);
      }
    }
    sitemap.end();
    Database.close();
  }

  async pages(){
    return (await Database
      .table('pages')
      .select('slug')
      .where('active', true))
      .map(p=>p.slug);
  }

  async system(){
    return [
      '/catalog',
    ];
  }

  async products(){
    const product_prefix = '/catalog/products/';
    return (await Database
      .table('products')
      .leftJoin('images', 'products.id', 'images.entity_id')
      .select(
        'products.slug as slug',
        'images.path as img_url'
      )
      .where('active', true))
      .map((item)=> {
        return {
          slug: `${product_prefix}${item.slug}`,
          img_url: `${Env.get('DOMAIN_NAME')}${item.img_url}`
        }
      });
  }
}

module.exports = new SiteMap();