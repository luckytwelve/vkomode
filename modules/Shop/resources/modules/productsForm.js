const ADM = require('../../../Adm/resources/assets/adm/adm');

ADM.modules.set('productForm', {
  tableSelector: '#productForm',
  slugDirty: false,

  init: function (model, ...args) {
    const that = this;
    this.slugDirty = !!$('[name=slug]', this).val();

    $('[name=title]', $(model)).on('keyup', function () {
      $('[name=slug]', $(model)).trigger('changed_name', $(this).val());
    });

    $('[name=slug]', $(model)).on('changed_name', function (e, val) {
      if (that.slugDirty) return;
      $(this).val(that.transliterate(val));
    });

    $('[name=slug]', $(model)).on('keyup', function (e, val) {
      that.slugDirty = true;
    });
  },

  transliterate(word) {
    const a = { 'Ё': 'YO', 'Й': 'I', 'Ц': 'TS', 'У': 'U', 'К': 'K', 'Е': 'E', 'Н': 'N', 'Г': 'G', 'Ш': 'SH', 'Щ': 'SCH', 'З': 'Z', 'Х': 'H', 'Ъ': '\'', 'ё': 'yo', 'й': 'i', 'ц': 'ts', 'у': 'u', 'к': 'k', 'е': 'e', 'н': 'n', 'г': 'g', 'ш': 'sh', 'щ': 'sch', 'з': 'z', 'х': 'h', 'ъ': '\'', 'Ф': 'F', 'Ы': 'I', 'В': 'V', 'А': 'a', 'П': 'P', 'Р': 'R', 'О': 'O', 'Л': 'L', 'Д': 'D', 'Ж': 'ZH', 'Э': 'E', 'ф': 'f', 'ы': 'i', 'в': 'v', 'а': 'a', 'п': 'p', 'р': 'r', 'о': 'o', 'л': 'l', 'д': 'd', 'ж': 'zh', 'э': 'e', 'Я': 'Ya', 'Ч': 'CH', 'С': 'S', 'М': 'M', 'И': 'I', 'Т': 'T', 'Ь': '\'', 'Б': 'B', 'Ю': 'YU', 'я': 'ya', 'ч': 'ch', 'с': 's', 'м': 'm', 'и': 'i', 'т': 't', 'ь': '\'', 'б': 'b', 'ю': 'yu' };
    return word.split('').map(function (char) {
      return a[char] || char;
    })
      .join('')
      .split(' ')
      .join('-')
      .replace(/[^a-zA-Z0-9-_]/gmi, '');
  }
});