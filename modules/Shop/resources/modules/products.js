/* eslint func-names: ["error", "never"] */
/* eslint prefer-arrow-callback: 0 */
/* global permissions, $ */
/* eslint object-shorthand: 0 */

const ADM = require('../../../Adm/resources/assets/adm/adm');


ADM.modules.set('products', {
  tableSelector: '#products',

  ReloadTable: (response, module) => {
    $(module.tableSelector).DataTable().ajax.reload(null, false);
  },

  AfterFormSend: (response, module) => {
    if (!response.notification) return;
    if (response.notification.type === 'success') {
      $('.modal').modal('hide');
      $(module.tableSelector).DataTable().ajax.reload(null, false);
    }
  },

  init: function (...args) {
    this.table = $(this.tableSelector).DataTable({
      order: [[0, 'desc']],
      dom: 'Bfrtip',
      buttons: [],
      // scrollY: '50vh',
      autoWidth: true,
      scrollX: '100%',
      scrollCollapse: true,

      processing: true,
      serverSide: true,
      initComplete: (oSettings) => {
        $(window).resize();
      },

      drawCallback: (oSettings) => {
        $(window).trigger('resize');
      },

      ajax: {
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: `${ADM.managerPath}/products/list`,
        type: 'POST',
        dataType: 'json',
      },

      columns: [
        { data: 'id', name: 'products.id' },
        {
          data: 'active',
          name: 'products.active',
          render: (data, type, row, meta) => {
            return data ? `<span class="kt-badge kt-badge--brand kt-badge--inline">Да</span>` : 'Нет';
          }
        },
        {
          data: 'name',
          name: 'products.title',
          render: (data, type, row, meta) => `${row.title}`
        },
        {
          data: 'price_base',
          orderable: true,
          searchable: true,
          name: 'products.price_base',
          render: (data, type, row, meta) => {
            return `${data} USD`
          }
        },
        {
          data: 'price_local',
          orderable: true,
          searchable: true,
          name: 'products.price_local',
          render: (data, type, row, meta) => {
            return `${data} UAH`
          }
        },
        {
          data: 'image', orderable: false, searchable: false, name: 'products.image',
          render: (data, type, row, meta) => {
            return `<img src="${data}" height="100">`
          }
        },

        {
          data: 'actions',
          name: 'actions',
          orderable: false,
          searchable: false,
          render: (data, type, row, meta) => {
            const actions = [];
            if (permissions.indexOf('products_edit')) {
              actions.push(`
              <a class="dropdown-item no-history" href="${ADM.managerPath}/products/edit/${row.id}" data-toggle="kt-tooltip" title="Tooltip title" data-placement="right" data-skin="dark" data-container="body">
                ${__('Adm.admin.edit')}
              </a>
              `);
            }

            actions.push(`
              <a class="dropdown-item no-history no-ajax" href="/catalog/products/${row.slug}" target="_blank">
                ${__('Adm.admin.view')}
              </a>
              `);

            if (permissions.indexOf('products_delete')) {
              actions.push(`
              <a class="dropdown-item no-history" href="${ADM.managerPath}/products/delete/${row.id}" data-callback="products::ReloadTable">
                ${__('Adm.admin.remove')}
              </a>
              `);
            }

            if (!actions.length) return '---';

            return `
              <button 
                class="btn btn-secondary dropdown-toggle" 
                type="button" 
                id="usersrrow" 
                data-toggle="dropdown" 
                aria-haspopup="true" 
                aria-expanded="false">
                ${__('Adm.admin.actions')}
              </button>
              <div class="dropdown-menu" aria-labelledby="usersrrow">
                ${actions.join('')}
              </div>
            `;
          }
        }
      ],
    });

  }
});

var substringMatcher = (strs) => {
  return function findMatches(q, cb) {
    var matches, substringRegex;

    matches = [];

    substrRegex = new RegExp(q, 'i');

    $.each(strs, function (i, str) {
      if (substrRegex.test(str)) {
        matches.push(str);
      }
    });

    cb(matches);
  };
};

