const ADM = require('../../../Adm/resources/assets/adm/adm');


ADM.modules.set('categories', {
  tableSelector: '#categories',

  ReloadTable: (response, module) => {
    $(module.tableSelector).DataTable().ajax.reload(null, false);
  },

  AfterFormSend: (response, module) => {
    if (!response.notification) return;
    if (response.notification.type === 'success') {
      $('.modal').modal('hide');
      $(module.tableSelector).DataTable().ajax.reload(null, false);
    }
  },

  init: function (...args) {
    this.table = $(this.tableSelector).DataTable({
      autoWidth: true,
      scrollX: '100%',
      scrollCollapse: true,
      pageLength: 50,
      processing: true,
      serverSide: true,

      initComplete: (oSettings) => {
        $(window).resize();
      },

      drawCallback: (oSettings) => {
        $(window).trigger('resize');
      },

      ajax: {
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: `${ADM.managerPath}/categories/list`,
        type: 'POST',
        dataType: 'json',
      },

      columns: [
        { data: 'id', name: 'categories.id' },
        { data: 'title', name: 'categories.title', orderable: true, searchable: true },
        { data: 'parent', name: 'categories.parent_id', orderable: false, searchable: true },
        {
          data: 'actions',
          name: 'actions',
          orderable: false,
          searchable: false,
          render: (data, type, row, meta) => {
            const actions = [];
            if (permissions.indexOf('categories_edit')) {
              actions.push(`
              <a class="dropdown-item no-history" href="${ADM.managerPath}/categories/edit/${row.id}" data-toggle="kt-tooltip" title="Tooltip title" data-placement="right" data-skin="dark" data-container="body">${__('Adm.admin.edit')}</a>
              
              `)
            }
            if (permissions.indexOf('categories_delete')) {
              actions.push(`
              <a class="dropdown-item no-history" href="${ADM.managerPath}/categories/delete/${row.id}" data-callback="users::ReloadTable">${__('Adm.admin.remove')}</a>
              `)
            }
            if (!actions.length) return '---';

            return `
            <button class="btn btn-secondary dropdown-toggle" type="button" id="usersrrow" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              ${__('Adm.admin.actions')}
            </button>
            <div class="dropdown-menu" aria-labelledby="usersrrow">
              ${actions.join('')}
            </div>
            `;
          }
        }
      ],
    });
  }
});
