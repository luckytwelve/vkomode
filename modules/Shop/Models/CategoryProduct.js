'use strict'

const Model = use('Model');

class CategoryProduct extends Model {
  static get table() {
    return 'categories_products';
  }
}

module.exports = CategoryProduct;
