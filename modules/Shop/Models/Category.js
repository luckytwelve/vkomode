'use strict'

const Model = use('Model');

class Category extends Model {
  static get table() {
    return 'categories';
  }

  parent() {
    return this.hasOne('Shop/Models/Category', 'parent', 'id');
  }
}

module.exports = Category;
