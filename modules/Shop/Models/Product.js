'use strict'

const Model = use('Model');

class Product extends Model {
  categories() {
    return this
      .belongsToMany('Shop/Models/Category')
      .pivotTable('categories_products');
  }

  mods() {
    return this.hasMany('Shop/Models/Category');
  }
}

module.exports = Product;
