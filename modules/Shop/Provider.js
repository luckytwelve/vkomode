'use strict'

const path = require('path');
// const fs = require('fs');
const { ServiceProvider } = require('@adonisjs/fold');

class Provider extends ServiceProvider {
  register() {
    const moduleName = path.dirname(__filename).split(path.sep).pop();
    // Bind Classes
    this.app.bind(`${moduleName}/Product`, () => {
      const ProductClass = require('./Classes/Product');
      return new ProductClass();
    });

    this.app.bind(`${moduleName}/Category`, () => {
      const CategoryClass = require('./Classes/Category');
      return new CategoryClass();
    });

  }

  boot() {
  }
}
module.exports = Provider;
