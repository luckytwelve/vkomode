'use strict'

module.exports = {
  'Products':
    [
      {
        'slug': 'products_view',
        'name': 'Products view'
      },
      {
        'slug': 'products_create',
        'name': 'Products create'
      },
      {
        'slug': 'products_edit',
        'name': 'Products edit'
      },
      {
        'slug': 'products_delete',
        'name': 'Products delete'
      }
    ]
,
  'Categories':
    [
      {
        'slug': 'categories_view',
        'name': 'Categories view'
      },
      {
        'slug': 'categories_create',
        'name': 'Categories create'
      },
      {
        'slug': 'categories_edit',
        'name': 'Categories edit'
      },
      {
        'slug': 'categories_delete',
        'name': 'Categories delete'
      }
    ]
}
