'use strict'
const Route = use('Route');

module.exports = [
  {
    index: 1,
    text: `
        <span class="svg-icon menu-icon">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <rect x="0" y="0" width="24" height="24"/>
                <path d="M14,9 L14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 L10,9 L8,9 L8,8 C8,5.790861 9.790861,4 12,4 C14.209139,4 16,5.790861 16,8 L16,9 L14,9 Z M14,9 L14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 L10,9 L8,9 L8,8 C8,5.790861 9.790861,4 12,4 C14.209139,4 16,5.790861 16,8 L16,9 L14,9 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                <path d="M6.84712709,9 L17.1528729,9 C17.6417121,9 18.0589022,9.35341304 18.1392668,9.83560101 L19.611867,18.671202 C19.7934571,19.7607427 19.0574178,20.7911977 17.9678771,20.9727878 C17.8592143,20.9908983 17.7492409,21 17.6390792,21 L6.36092084,21 C5.25635134,21 4.36092084,20.1045695 4.36092084,19 C4.36092084,18.8898383 4.37002252,18.7798649 4.388133,18.671202 L5.86073316,9.83560101 C5.94109783,9.35341304 6.35828794,9 6.84712709,9 Z" fill="#000000"/>
            </g>
          </svg>
        </span>
        <span class="menu-text">{{Shop.general.shop}}</span>
        <i class="menu-arrow"></i>`,

    'a.href': 'javascript:;',
    'li.class': 'menu-item menu-item-submenu',
    'a.class': 'menu-link menu-toggle',
    'li.data-menu-toggle': 'hover',
    'li.aria-haspopup': 'true',
    'ul.class': 'menu-subnav',
    permissions: [
      'products_view',
      'category_view'
    ],
    children: [
      {
        'li.class': 'menu-item',
        'li.aria-haspopup': 'true',
        'a.class': 'menu-link',
        text: `
            <i class="menu-bullet menu-bullet-dot"><span></span></i>
            <span class="menu-text">{{Shop.products.products}}</span>`,
        'a.href': () => { return Route.url('admin.products.list') },
        permissions: [
          'products_view'
        ]
      },
      {
        'li.class': 'menu-item',
        'li.aria-haspopup': 'true',
        'a.class': 'menu-link',
        text: `
            <i class="menu-bullet menu-bullet-dot"><span></span></i>
            <span class="menu-text">{{Shop.categories.categories}}</span>`,
        'a.href': () => { return Route.url('admin.categories.list') },
        permissions: [
          'products_view'
        ]
      },
    ]
  }
];
