const Config = use('Config');
const Setting = use('Settings/Models/Setting');
const Input = use('Settings/utils/Input');
const Notify = use('ADM/Notify');

class SettingsController {
  async index({ view, __ }) {
    const settingsList = Config.get('admin.settings');
    const settings = (await Setting.all()).toJSON()
      .reduce((a, c) => (
        {
          ...a,
          [c.name]: c.value
        }), {});

    const tabs = [];
    const tabpanels = [];
    if (settingsList) {
      settingsList.forEach((item, key) => {
        tabs.push(`
          <li class="nav-item">
            <a class="nav-link ${key === 0 ? 'active' : ''}" data-toggle="tab" href="#" data-target="#tab_${key}">${__(item.text)}</a>
          </li>`);

        const content = [];
        item.childs.forEach((child) => {
          content.push(`
          <div class="form-group row">
            <label class="col-xl-3 col-lg-3 col-form-label">${__(child.text)}</label>
            <div class="col-lg-9 col-xl-9">
            ${Input.get(child, settings[child.name])}
            ${child.description ? `<span class="form-text text-muted">${__(child.description)}</span>` : ''}
            </div>
          </div>
          `);
        });
        
        tabpanels.push(`
        <div class="tab-pane ${key === 0 ? 'active' : ''}" id="tab_${key}" role="tabpanel">
          ${content.join('')}
        </div>`);
      });
    }

    return view.render('Settings.lists', {
      tabs: tabs.join(''),
      tabpanels: tabpanels.join('')
    });
  }


  save({ request, response }) {
    const input = request.all();
    Object.entries(input).forEach(async (item) => {
      console.log(item)
      const [name, value] = item;
      let setting = await Setting.findBy('name', name);

      if (!setting) {
        setting = new Setting();
      }

      setting.name = name;
      setting.value = Array.isArray(value) ? JSON.stringify(value) : value;
      await setting.save();
    });

    return response.json(Notify.success('Настройки сохранены', {}));
  }
}

module.exports = SettingsController;
