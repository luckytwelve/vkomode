module.exports = [
  {

    index: 1,
    text: 'Melin.admin.main',
    permissions: [],
    childs: [
      {
        name: 'favorite_products',
        class: 'form-control',
        text: 'Melin.admin.favorite_products',
        description: 'Melin.admin.favorite_products_description',
        type: 'text',
        defaultValue: '',
        script: ''
      },
      {
        name: 'new_products',
        class: 'form-control',
        text: 'Melin.admin.new_products',
        description: 'Melin.admin.new_products_description',
        type: 'text',
        defaultValue: '',
        script: ''
      },
      {
        name: 'category_main',
        class: 'form-control',
        text: 'Melin.admin.categories_main',
        description: 'Melin.admin.categories_main_description',
        type: 'text',
        defaultValue: '',
        script: ''
      },

      {
        name: 'intro_text',
        class: 'form-control editor',
        text: 'Melin.admin.intro_text',
        type: 'text',
        defaultValue: '',
        script: ''
      },

      {
        name: 'description_text',
        class: 'form-control editor',
        text: 'Melin.admin.description_text',
        type: 'text',
        defaultValue: '',
        script: ''
      },
    ],
  },
  // {
  //   index: 2,
  //   text: 'Adm.admin.info_blocks',
  //   permissions: [],
  //   childs: [
  //     {
  //       name: 'top_banner_left',
  //       class: 'styled form-control',
  //       text: 'Adm.admin.top_banner_left',
  //       type: 'textarea',
  //       defaultValue: 'Forever Forest Magical Christmas Event has Arrived',
  //       script: ''
  //     },
  //     {
  //       name: 'top_banner_right',
  //       class: 'styled form-control editor',
  //       text: 'Adm.admin.top_banner_right',
  //       type: 'textarea',
  //       defaultValue: '<a href="tel:(609) 521-2373" class="top-bar__link">For Questions & Special Offers - Send us a Text Today @ (609) 521-2373</a>',
  //       script: ''
  //     },
  //     {
  //       name: 'join_us_block',
  //       class: 'styled form-control',
  //       text: 'Adm.admin.join_us_block',
  //       type: 'textarea',
  //       width: "800px",
  //       height: "100px",
  //       defaultValue: 'Join us for a magical christmas experience \n' +
  //         '                            forever forest winter christmas event this winter 2020',
  //       script: ''
  //     },
  //     {
  //       name: 'select_dates_block',
  //       class: 'styled form-control editor col-lg-9 col-xl-9',
  //       text: 'Adm.admin.select_dates_block',
  //       type: 'textarea',
  //       width: "800px",
  //       height: "100px",
  //       defaultValue: 'November 8th <br>\n' +
  //         '                                                - December 29th',
  //       script: ''
  //     },
  //     {
  //       name: 'limited_tickets_block',
  //       class: 'styled form-control editor',
  //       text: 'Adm.admin.limited_tickets_block',
  //       type: 'textarea',
  //       width: "800px",
  //       height: "100px",
  //       defaultValue: 'Tickets are limited. <br>\n' +
  //         '                                                Don\'t Delay!',
  //       script: ''
  //     },
  //     {
  //       name: 'count_down_block',
  //       class: 'styled form-control editor',
  //       text: 'Adm.admin.count_down_block',
  //       type: 'textarea',
  //       width: "800px",
  //       height: "100px",
  //       defaultValue: 'Count Down Until Christmas\n' +
  //         '                                                Day!',
  //       script: ''
  //     },
  //     {
  //       name: 'book_today_header_block',
  //       class: 'styled form-control',
  //       text: 'Adm.admin.book_today_header_block',
  //       type: 'textarea',
  //       width: "800px",
  //       height: "100px",
  //       defaultValue: 'To book your 2020 Winter Forever Forest Christmas Event Admission Tickets, please use the form below and reserve your date.',
  //       script: ''
  //     },
  //     {
  //       name: 'book_today_content_block',
  //       class: 'styled form-control editor',
  //       text: 'Adm.admin.book_today_content_block',
  //       type: 'textarea',
  //       width: "800px",
  //       height: "100px",
  //       defaultValue: '<div class="book__options-list">\n' +
  //         '                        <div class="book-option">\n' +
  //         '                            <div class="book-option__number">01</div>\n' +
  //         '                            <div class="book-option__title">Featured <br>\n' +
  //         '                                Attractions\n' +
  //         '                            </div>\n' +
  //         '                            <ul class="list">\n' +
  //         '                                <li>Christmas Tree Walking Trail\n' +
  //         '                                    of Wonder\n' +
  //         '                                </li>\n' +
  //         '                                <li>Magic of Christmas Barn - Featuring Nate</li>\n' +
  //         '                                <li>Unworth nightly</li>\n' +
  //         '                                <li>Selfie-station Barn</li>\n' +
  //         '                                <li>Visits with Santa Clause and Photo\'s with Santa</li>\n' +
  //         '                                <li>Old-fashioned Hayride</li>\n' +
  //         '                                <li>Head Elf Mancini\'s Santa Workshop with Arts and Crafts + Toy testing</li>\n' +
  //         '                                <li>Outdoor Christmas Themed Activities</li>\n' +
  //         '                                <li><b>Free Parking on Premises, newly renovated</b></li>\n' +
  //         '                            </ul>\n' +
  //         '                        </div>\n' +
  //         '                        <div class="book-option">\n' +
  //         '                            <div class="book-option__number">02</div>\n' +
  //         '                            <div class="book-option__title">Mancini Workshop <br>\n' +
  //         '                                Include\n' +
  //         '                            </div>\n' +
  //         '                            <ul class="list">\n' +
  //         '                                <li>Various Toy Testing with Santa\'s Elves</li>\n' +
  //         '                                <li>Face Painting</li>\n' +
  //         '                                <li>Nail Polishing</li>\n' +
  //         '                                <li>Letters to Santa</li>\n' +
  //         '                                <li>Arts & crafts</li>\n' +
  //         '                                <li>Nintendo Switch</li>\n' +
  //         '                                <li>Virtual Reality Slay-Ride</li>\n' +
  //         '                                <li>Santa and his elves</li>\n' +
  //         '                                <li>Carnival-theme\'d laser target practice range</li>\n' +
  //         '                            </ul>\n' +
  //         '                        </div>\n' +
  //         '                        <div class="book-option">\n' +
  //         '                            <div class="book-option__number">03</div>\n' +
  //         '                            <div class="book-option__title">Outside Activities <br>\n' +
  //         '                                Include\n' +
  //         '                            </div>\n' +
  //         '                            <ul class="list">\n' +
  //         '                                <li>Christmas Tree Walking Trail\n' +
  //         '                                    of Wonder\n' +
  //         '                                </li>\n' +
  //         '                                <li>Old-fashioned hay ride</li>\n' +
  //         '                                <li>Bounce Castle</li>\n' +
  //         '                                <li>Inflatable live-in snow globes</li>\n' +
  //         '                                <li>MarshMello Lane, Outdoor movie screen and fire pits\n' +
  //         '                                    <b>(Roast your own marshmallow\'s)</b>\n' +
  //         '                                </li>\n' +
  //         '                                <li>Music played outdoor and in Mancini workshop</li>\n' +
  //         '                                <li>Jolly Food Court for snacks and drinks</li>\n' +
  //         '                                <li>Grinch Dunk Tank</li>\n' +
  //         '                                <li>Trees, Lights, and many decorations.</li>\n' +
  //         '                            </ul>\n' +
  //         '                        </div>\n' +
  //         '                    </div>',
  //       script: ''
  //     },
  //     {
  //       name: 'select_a_date_block',
  //       class: 'styled form-control',
  //       text: 'Adm.admin.select_a_date_block',
  //       type: 'textarea',
  //       width: "800px",
  //       height: "100px",
  //       defaultValue: 'If you have any issue\'s with the ticket selection widget below, click the button below to use our direct ticket reservation page',
  //       script: ''
  //     },
  //     {
  //       name: 'located_in_block',
  //       class: 'styled form-control editor',
  //       text: 'Adm.admin.located_in_block',
  //       type: 'textarea',
  //       width: "800px",
  //       height: "100px",
  //       defaultValue: '<h2 class="section-title contacts__title">New Jersey</h2>\n' +
  //         '                        <div class="section-subtitle contacts__text">354 Bremen Ave Egg Harbor City, NJ 08215 (Off route 30)</div>',
  //       script: ''
  //     },
  //     {
  //       name: 'how_to_prepare_block',
  //       class: 'styled form-control editor',
  //       text: 'Adm.admin.how_to_prepare_block',
  //       type: 'textarea',
  //       width: "800px",
  //       height: "100px",
  //       defaultValue: '<ul class="list list--white">\n' +
  //         '                                <li>Select and reserve your ticket date and time range, we currently have two shifts on most of our available dates! 12PM to 4PM and 5PM to 9PM - You can see more details on ticket packages on our Reservation Page.</li>\n' +
  //         '                                <li>Our winter event will cater to family and friends! Be ready for indoor/outdoor activities and be sure your family/friends enjoy the Christmas Holiday Spirit, after all we are a Christmas tree farm!</li>\n' +
  //         '                                <li>Dress appropriately for the weather. On rainy days, be sure to wear boats and stay warm!</li>\n' +
  //         '                                <li>Your tickets are transferable to another day for a rain check, or unexpected event. Feel free to call or text us with any questions or concerns! You will find our contact details below.</li>\n' +
  //         '                            </ul>',
  //       script: ''
  //     },
  //     {
  //       name: 'get_admission_block',
  //       class: 'styled form-control editor',
  //       text: 'Adm.admin.get_admission_block',
  //       type: 'textarea',
  //       width: "800px",
  //       height: "100px",
  //       defaultValue: '<h2 class="section-title general-form__title">Get 10% off admission</h2>\n' +
  //         '                        <h4 class="section-subtitle">Sign up to receive exclusive offers and get 10% off Forever Forest\n' +
  //         '                            Admission for the 2020 Season</h4>',
  //       script: ''
  //     },
  //   ],
  // },
  // {
  //   index: 3,
  //   text: 'Adm.admin.profile_cap',
  //   permissions: [],
  //   childs: [
  //     {
  //       name: 'cap_state',
  //       class: '',
  //       text: 'Adm.admin.cap_state',
  //       type: 'radio',
  //       values: [
  //         { text: 'Enabled', value: 'enabled' },
  //         { text: 'Disabled', value: 'disabled' },
  //       ],
  //       script: ''
  //     },
  //     {
  //       name: 'cap_text',
  //       class: 'styled form-control',
  //       text: 'Adm.admin.cap_text',
  //       type: 'textarea',
  //       width: "800px",
  //       height: "100px",
  //       defaultValue: 'Coming soon…',
  //       script: ''
  //     },
  //     {
  //       name: 'cap_img',
  //       class: 'styled form-control',
  //       text: 'Adm.admin.cap_img',
  //       type: 'file',
  //       defaultValue: '',
  //       script: ''
  //     }
  //   ],
  // }
]

