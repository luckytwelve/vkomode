module.exports = [
  {
    index: 1,
    text: 'Settings.admin.main',
    permissions: [],
    childs: [
      {
        name: 'dollar_rate',
        class: 'form-control',
        text: 'Settings.admin.dollar_rate',
        type: 'text',
        defaultValue: '28.1',
        script: ''
      },
      {
        name: 'operation_time',
        class: 'form-control',
        text: 'Settings.admin.operation_time',
        type: 'text',
        defaultValue: 'с 10:00 до 19:00',
        script: ''
      },
      {
        name: 'sales_department',
        class: 'form-control',
        text: 'Settings.admin.sales_department',
        type: 'text',
        defaultValue: '+380675824431; +380678075239',
        script: ''
      },
      {
        name: 'manager_email',
        class: 'form-control',
        text: 'Settings.admin.manager_email',
        type: 'text',
        defaultValue: 'vkomode@gmail.com',
        script: ''
      },
    ],
  }
]

