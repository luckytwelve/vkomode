const Route = use('Route');

module.exports = [
  {
    index: 3,
    text: `<span class="svg-icon menu-icon">   
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <rect x="0" y="0" width="24" height="24"/>
            <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"/>
        </g> 
        </svg>
        </span>
        <span class="menu-text">{{Adm.admin.settings}}</span>`,
    'a.href': 'admin.settings',
    'li.class': 'menu-item',
    'a.class': 'menu-link menu-toggle',
    'li.data-menu-toggle': 'hover',
    'li.aria-haspopup': 'true',
    'ul.class': '',
    permissions: ['settings_view'],
    children: []
  },
  {
    index: 6,
    text: `<span class="svg-icon menu-icon">   
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path d="M3.5,21 L20.5,21 C21.3284271,21 22,20.3284271 22,19.5 L22,8.5 C22,7.67157288 21.3284271,7 20.5,7 L10,7 L7.43933983,4.43933983 C7.15803526,4.15803526 6.77650439,4 6.37867966,4 L3.5,4 C2.67157288,4 2,4.67157288 2,5.5 L2,19.5 C2,20.3284271 2.67157288,21 3.5,21 Z" fill="#000000" opacity="0.3"/>
        <polygon fill="#000000" opacity="0.3" points="4 19 10 11 16 19"/>
        <polygon fill="#000000" points="11 19 15 14 19 19"/>
        <path d="M18,12 C18.8284271,12 19.5,11.3284271 19.5,10.5 C19.5,9.67157288 18.8284271,9 18,9 C17.1715729,9 16.5,9.67157288 16.5,10.5 C16.5,11.3284271 17.1715729,12 18,12 Z" fill="#000000" opacity="0.3"/>
    </g>
        </svg>
        </span>
        <span class="menu-text">{{Adm.admin.file_manager}}</span>`,
    'li.class': 'menu-item',
    'a.class': 'menu-link menu-toggle filemanager filemanager-open no-history',
    'li.data-menu-toggle': 'hover',
    'li.aria-haspopup': 'true',
    'ul.class': '',
    permissions: ['settings_view'],
    children: []
  }
];
