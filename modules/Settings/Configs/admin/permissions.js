module.exports = {
  Settings:
    [
      {
        slug: 'settings_view',
        name: 'Settings view'
      },
      {
        slug: 'settings_edit',
        name: 'Settings edit'
      },
    ],
};
