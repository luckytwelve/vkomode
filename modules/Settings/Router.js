const Route = use('Route');

Route.group(() => {
  Route
    .any('settings', '@provider:Settings/Controllers/SettingsController.index')
    .as('admin.settings')
    .middleware(['managerXHR', 'managerAuth']);

  Route
    .post('settings/save', '@provider:Settings/Controllers/SettingsController.save')
    .as('admin.settings.save')
    .middleware(['managerXHR', 'managerAuth']);
}).prefix('admin');
