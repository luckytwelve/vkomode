class Setting {
  get(name, settings) {
    if (settings.length) {
      const item = settings.find(i => i.name === name);
      return item ? item.value : '';
    }
    return '';
  }
}

module.exports = new Setting();
