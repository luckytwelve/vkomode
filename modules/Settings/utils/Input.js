class Input {
  get(setting, value) {
    const obj = {
      name: '',
      text: '',
      type: '',
      defaultValue: '',
      class: '',
      attributes: '',
      script: '',
      'area-size': '',
      content: '',
      'filemanager-id': '',
      values: [],
      ...setting
    };

    if (typeof obj.values === 'function') {
      obj.values = obj.values();
    }

    return this[obj.type](obj, value || '');
  }

  checkbox(setting, value) {
    const array = [];

    if (setting.values.length) {
      setting.values.forEach((item) => {
        let checked = '';
        if (value.length && Array.isArray(JSON.parse(value))) {
          checked = value.includes(item.value) ? 'checked' : '';
        }
        const checkbox = `<label>
            <input
              class="${setting.class}"
              type="checkbox"
              name="${setting.name}[]"
              value="${item.value}" ${checked}> ${item['text']}</label><br/>`;
        array.push(checkbox);
      });
    }
    return array.join('');
  }

  link() {

  }

  radio(setting, value) {
    const array = [];
    if (setting.values.length) {
      setting.values.forEach((item) => {
        const checked = item.value === value ? 'checked' : '';
        const checkbox = `<label>
            <input
              class="${setting.class}"
              type="radio"
              name="${setting.name}"
              value="${item.value}" ${checked}> ${item.text}</label><br/>`;
        array.push(checkbox);
      });
    }
    return `<div class="col-lg-6 col-xl-6">${array.join('')}</div>`;
  }

  select(setting, value) {
    const options = [];
    if (setting.values.length) {
      setting.values.forEach((i) => {
        const item = typeof i !== 'object'
          ? { text: i, value: i }
          : i;
        const selected = item.value === value ? 'selected' : '';
        options.push(`<option value="${item.value}" ${selected} >${item.text}</option>`);
      });
    }
    return `<select class="${setting.class}" name="${setting.name}">${options.join('')}</select>`;
  }

  text(setting, value) {
    return `
      <input 
        class="${setting.class}" 
        id="${setting['filemanager-id']}" 
        title="${setting.text}" 
        type="text" 
        name="${setting.name}" 
        value="${!(value.trim().length) ? setting.defaultValue : value}"
      >`;
  }

  textarea(setting, value) {
    return `
      <textarea 
        style="width:${setting.width};height:${setting.height}"
        id="${setting.id === undefined ? `texarea-${setting.name}` : setting.id}"
        ${setting['data-ui'] !== undefined && setting['data-ui'] === 'ckeditor' ? 'data-ui=\'ckeditor\'' : ''}
        class="${setting.class}"
        name="${setting.name}"
        ${setting['area-size']}>${value.trim().length ? value : setting.defaultValue}</textarea>`;
  }

  file(setting, value) {
    return `
    <div class="form-group row">
      <div class="col-lg-9 col-xl-9">
        <input class="form-control form-control-solid" name="${setting.name}" type="file" value="">
        <img src="/uploads/settings/${value}" style="width:200px;">
      </div>
    </div>`;
  }
}

module.exports = new Input();
