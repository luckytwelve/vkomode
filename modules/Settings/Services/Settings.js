const { ServiceProvider } = require('@adonisjs/fold');

class Settings extends ServiceProvider {
  register() {
    let settings = {};

    this.app.bind('ADM/Settings', () => {
      const Setting = use('Settings/Models/Setting');

      return {
        all: () => settings,
        get: name => settings[name],
        load: async () => {
          (await Setting.all()).toJSON().forEach((s) => { settings[s.name] = s.value; });
        }
      };
    });
  }

  boot() {
    const Server = this.app.use('Adonis/Src/Server');

    Server.registerNamed({
      settings: 'Settings/Middleware/Settings'
    });
  }
}

module.exports = Settings;
