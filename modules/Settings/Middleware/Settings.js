
class Locale {
  async handle({ view }, next) {
    const View = use('View');
    const Settings = use('ADM/Settings');
    await Settings.load();

    View.global('settings', (name) => {
      console.log(name, Settings.get(name))
      return Settings.get(name);
    });
    view.share({ settings: Settings.all() });
    await next();
  }
}
module.exports = Locale;
