const Database = use('Database');


class MainPageProducts{

  async specialProducst(){

    let special_products = await Database
      .table('products')
      .select(
        'products.id',
        'products.slug',
        'products.title',
        'products.price_local'
      )
      .where('active', true)
      .where('special_offer', true)
    const special_products_images = await Database
      .table('images')
      .select(
        'images.path',
        'images.entity_id'
      )
      .where('entity', 'product')
      .whereIn('entity_id', special_products.map(i=>Number(i.id)))

    special_products = special_products.map((c) => {
      const images = special_products_images.filter(i => i.entity_id === c.id);
      return {
        ...c,
        image: images ? images[0].path : ''
      }
    });
    const s_products = [];
    const size = 4;
    for (let i=0; i<special_products.length; i+=size) {
      s_products.push(special_products.slice(i,i+size));
    }
    return s_products;
  }

  async lastProducts(){
    const size = 4;
    let last_products = await Database
      .table('products')
      .select(
        'products.id',
        'products.slug',
        'products.title',
        'products.price_local'
      )
      .where('active', true)
      .orderBy('id', 'desc')
      .limit(8)

    let last_products_moods = await Database
      .table('product_mods')
      .select('id', 'product_id')
      .whereIn('product_id', last_products.map(i=>Number(i.id)));

    let last_products_mods_images = await Database
      .table('images')
      .select(
        'images.path',
        'images.entity_id'
      )
      .where('entity', 'product_mod')
      .whereIn('entity_id', last_products_moods.map(i=>Number(i.id)))

    last_products_mods_images = last_products_mods_images.map((image)=>{
      let mod = last_products_moods.filter(p_mod => Number(p_mod.id) === Number(image.entity_id))[0]
      return {
        path: image.path,
        entity_id: mod.product_id
      }
    });

    let last_products_images = await Database
      .table('images')
      .select(
        'images.path',
        'images.entity_id'
      )
      .where('entity', 'product')
      .whereIn('entity_id', last_products.map(i=>Number(i.id)))

    last_products_images = last_products_mods_images.length ? last_products_images.concat(last_products_mods_images) : last_products_images;
    last_products = last_products.map((c) => {
      const images = last_products_images.filter(i => i.entity_id === c.id);
      return {
        ...c,
        image_front: images && images[0] ? images[0].path : '',
        image_back: images && images[1] ? images[1].path : images[0].path,
      }
    });
    const l_products = [];

    for (let i=0; i<last_products.length; i+=size) {
      l_products.push(last_products.slice(i,i+size));
    }

    return l_products;
  }

  async bestProducts(){
    let best_products = await Database
      .table('products')
      .select(
        'products.id',
        'products.slug',
        'products.title',
        'products.price_local'
      )
      .where('active', true)
      .where('best_product', true)

    const best_products_images = await Database
      .table('images')
      .select(
        'images.path',
        'images.entity_id'
      )
      .where('entity', 'product')
      .whereIn('entity_id', best_products.map(i=>Number(i.id)))

    best_products = best_products.map((c) => {
      const images = best_products_images.filter(i => i.entity_id === c.id);
      return {
        ...c,
        image: images ? images[0].path : ''
      }
    });
    return best_products;
  }

  async viwedProducts(request){
    const viewed_products_ids = request.cookie('viewed_products', [])
    const viewed_size = 6;
    let viewed_products = await Database
      .table('products')
      .select(
        'products.id',
        'products.slug',
        'products.title'
      )
      .where('active', true)
      .whereIn('id', viewed_products_ids)

    const viewed_products_images = await Database
      .table('images')
      .select(
        'images.path',
        'images.entity_id'
      )
      .where('entity', 'product')
      .whereIn('entity_id', viewed_products.map(i=>Number(i.id)))

    viewed_products = viewed_products.map((c) => {
      const images = viewed_products_images.filter(i => i.entity_id === c.id);
      return {
        ...c,
        image: images ? images[0].path : ''
      }
    });

    const w_products = [];
    for (let i=0; i<viewed_products.length; i+=viewed_size) {
      w_products.push(viewed_products.slice(i,i+viewed_size));
    }
    return w_products
  }

  async randomProducts(products){
    return this.removeDuplicates(products, 'id').slice(0, 6);
  }

  removeDuplicates(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }
}

module.exports = new MainPageProducts();