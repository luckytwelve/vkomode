const Seo = use('Pages/Models/Seo');

class Meta {

  async get(slug, custom ){
    if(custom) return custom;
    let meta = await Seo
      .query()
      .where('slug', slug)
      .first();
    return meta ? meta.toJSON() : {
      meta_title: 'Vkomode - Платки, шарфи, опт, рознца, отправка Новой почтой 1-2 дня.',
      meta_description: 'Vkomode - Платки, шарфи, опт, рознца, отправка Новой почтой 1-2 дня.',
      meta_keywords: 'Платки, шарфи, опт, рознца, Хмельницкий, Украина, отправка Новой почтой 1-2 дня'
    }
  }
}

module.exports = new Meta();