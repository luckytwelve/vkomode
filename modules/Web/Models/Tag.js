'use strict'

const Model = use('Model');

class Tag extends Model {
  static get table() {
    return 'tags';
  }
}

module.exports = Tag;
