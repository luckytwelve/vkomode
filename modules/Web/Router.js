const Route = use('Route');

Route.group(() => {

  Route
    .get('/', '@provider:Web/Controllers/IndexController.index')
    .as('web.index').middleware(['settings', 'session']);

  Route
    .get('/catalog', '@provider:Web/Controllers/CatalogController.index')
    .as('web.catalog').middleware(['settings', 'session']);

  Route
    .get('/catalog/products/:slug', '@provider:Web/Controllers/ProductController.index')
    .as('web.product').middleware(['settings', 'session']);

  Route
    .post('/delete-recent-viewed', '@provider:Web/Controllers/ProductController.deleteRecentViewed')
    .as('web.delete-recent-viewed').middleware(['settings', 'session']);

  Route
    .get('/informatsiya', '@provider:Web/Controllers/PagesController.show' )
    .as('web.static-page').middleware(['settings', 'session']);

  Route
    .get('/aktsii', '@provider:Web/Controllers/PagesController.show' )
    .as('web.static-page').middleware(['settings', 'session']);

  Route
    .get('/o-nas', '@provider:Web/Controllers/PagesController.show' )
    .as('web.static-page').middleware(['settings', 'session']);

  Route
    .get('/kontakti', '@provider:Web/Controllers/PagesController.show' )
    .as('web.static-page').middleware(['settings', 'session']);

  Route
    .get('/oplata', '@provider:Web/Controllers/PagesController.show' )
    .as('web.static-page').middleware(['settings', 'session']);

  Route
    .get('/informatsiya-o-dostavke', '@provider:Web/Controllers/PagesController.show' )
    .as('web.static-page').middleware(['settings', 'session']);

  Route
    .get('/tablitsa-razmerov', '@provider:Web/Controllers/PagesController.show' )
    .as('web.static-page').middleware(['settings', 'session']);

  Route
    .get('/search', '@provider:Web/Controllers/SearchController.search')
    .as('web.search').middleware(['settings', 'session']);

  Route
    .post('/cart/add/:id', '@provider:Web/Controllers/CartController.add')
    .as('add.to.cart').middleware(['session'])

  Route
    .post('/cart/delete/:id', '@provider:Web/Controllers/CartController.delete')
    .as('delete.from.cart').middleware(['session'])

  Route
    .post('/cart/quantity/change/:id', '@provider:Web/Controllers/CartController.changeQuantity')
    .as('change.quantity.cart.product').middleware(['session'])

  Route
    .get('/order', '@provider:Web/Controllers/CartController.order')
    .as('web.order').middleware(['settings', 'session'])

  Route
    .get('/cart', '@provider:Web/Controllers/CartController.show')
    .as('show.cart').middleware(['settings', 'session'])

});

