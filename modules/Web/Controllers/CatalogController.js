'use strict'

const Database = use('Database');
const moment = require('moment');
const Meta = use('Web/Classes/Meta');

class CatalogController {
  async index({ request, view }) {

    const meta = await Meta.get(request.url());

    const categories = await Database
      .table('categories')
      .select(
        'id',
        'title',
        'order',
        'slug'
      )
      .where('active', true)
      .orderBy('order', 'asc')

    let products = await Database
      .table('products')
      .select(
        'products.id',
        'products.slug',
        'products.title',
        'products.price_local',
        'categories_products.category_id',
        'products.created_at'
      )
      .leftJoin('categories_products', 'products.id', 'categories_products.product_id')
      .where('active', true)

    products.map((product)=>{
      product.created_at = moment(product.created_at).format('MMMM M, YY')
      return product
    });

    const prices = products.map(product=>Number(product.price_local));
    const min_price = prices.length ? Math.min.apply(null, prices) : 1;
    const max_price = prices.length ? Math.max.apply(null, prices) : 1;

    const products_images = await Database
      .table('images')
      .select(
        'images.path',
        'images.entity_id'
      )
      .where('entity', 'product')
      .whereIn('entity_id', products.map(i=>Number(i.id)))

    products = products.map((c) => {
      const images = products_images.filter(i => i.entity_id === c.id);
      return {
        ...c,
        image: images ? images[0].path : ''
      }
    });

    return view.render('Web.pages.catalog', {
      categories,
      products,
      min_price,
      max_price,
      ...meta,
      slug: request.url()
    })
  }
}

module.exports = CatalogController;
