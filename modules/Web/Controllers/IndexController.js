'use strict'

const Meta = use('Web/Classes/Meta');
const MainPageProducts = use('Web/Classes/MainPageProducts');

class IndexController {
  async index({ request, view }) {

    const meta = await Meta.get(request.url());

    const special_products = await MainPageProducts.specialProducst();
    const last_products = await MainPageProducts.lastProducts();
    const best_products = await MainPageProducts.bestProducts();
    const viewed_products = await MainPageProducts.viwedProducts(request);

    return view.render('Web.pages.index', {
      special_products,
      last_products,
      best_products,
      viewed_products,
      //random_products,
      ...meta,
      slug: request.url()
    });
  }
}

module.exports = IndexController;
