const Database = use('Database');

class SearchController{
  async search({ params, request, view}){
    const { name } = request.get();
    let products = [];
    if(name.trim().length){
      let search_chunks = name.trim().split(' ')
      products = Database
        .table('products')
        .select(
          'products.id',
          'products.slug',
          'products.title',
          'products.price_local'
        )
        .where((builder) => {
          if (search_chunks.length) {
            for(let i =0; i < search_chunks.length; i++){
              if(!i){
                builder.whereRaw('LOWER(products.title) LIKE ?', [`%${search_chunks[i].toLowerCase()}%`])
                builder.orWhereRaw('LOWER(products.description) LIKE ?' ,[`%${search_chunks[i].toLowerCase()}%`])
              } else {
                builder.orWhereRaw('LOWER(products.title) LIKE ?', [`%${search_chunks[i].toLowerCase()}%`])
                builder.orWhereRaw('LOWER(products.description) LIKE ?', [`%${search_chunks[i].toLowerCase()}%`])
              }
            }
          }
          builder.where('products.active', '=', true)
        })
      products = await products;
    }

    const products_images = await Database
      .table('images')
      .select(
        'images.path',
        'images.entity_id'
      )
      .where('entity', 'product')
      .whereIn('entity_id', products.map(i=>Number(i.id)))

    products = products.map((c) => {
      const images = products_images.filter(i => i.entity_id === c.id);
      return {
        ...c,
        image: images ? images[0].path : ''
      }
    });


    return view.render('Web.pages.search', {
      products,
      is_found : !!products.length,
      slug: request.url(),
      name
    });
  }
}

module.exports = SearchController;