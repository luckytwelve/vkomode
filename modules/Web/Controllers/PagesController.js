const Page = use('Pages/Models/Page');
const Meta = use('Web/Classes/Meta');

class PagesController {

  async show({ params, request, view }){
    const meta = await Meta.get(request.url());
    let page = await Page
      .query()
      .where('slug', `${request.url().trim().toLowerCase()}`)
      .where('active', true)
      .first();
    page = page ? page.toJSON() : {
      title: 'Страцина в процесе обновления',
      description: 'Страцина в процесе обновления, повторите запрос позже.'
    }

    return view.render('Web.pages.page', {
      ...page,
      ...meta,
      slug: request.url()
    });
  }
}

module.exports = PagesController