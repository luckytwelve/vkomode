'use strict'

const ProductRepository = use('Shop/Repositories/Product');
const Meta = use('Web/Classes/Meta');

class ProductController {
  async index({ request, view, params, response }) {

    const { slug } = params;
    try {
      const product = await ProductRepository.getByAlias(slug);
      let viewed_products = request.cookie('viewed_products', [])
      if(!viewed_products.includes(Number(product.id))){
        viewed_products.push(Number(product.id))
      }
      response.cookie('viewed_products', viewed_products, { httpOnly: true, path: '/' })

      product.images = [];
      product.images.push(product.image[0].path)
      product.mods.map((m)=>{
        product.images.push(m.image)
      })
      return view.render('Web.pages.product', {
        product,
        ...product.meta,
        slug: request.url()
      });
    } catch (e) {
      console.log(e)
      return view.render('Web.404');
    }
  }

  async deleteRecentViewed({request, response}){
    const input = request.only(['id'])
    let viewed_products = request.cookie('viewed_products', [])
    if(viewed_products.includes(Number(input.id))){
      const index = viewed_products.indexOf(Number(input.id));
      if (index > -1) {
        viewed_products.splice(index, 1);
        response.cookie('viewed_products', viewed_products, { httpOnly: true, path: '/' })
      }
    }
    return response.status(200).json({
      id: Number(input.id),
      success: true
    })
  }
}

module.exports = ProductController;
