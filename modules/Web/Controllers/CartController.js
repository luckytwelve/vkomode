const Database = use('Database');
const View = use('View');
const Meta = use('Web/Classes/Meta');

class CartController {

  async add({params, request, session, response}){
    const {id} = params;
    const products = session.get('products', []);
    const {p_quantity} = request.all();

    let product = await Database
      .table('products')
      .select(
        'products.id',
        'products.slug',
        'products.title',
        'products.price_local'
      )
      .where('active', true)
      .where('id', Number(id))
      .first()

    const image = await Database
      .table('images')
      .select(
        'images.path',
        'images.entity_id'
      )
      .where('entity', 'product')
      .where('entity_id', Number(id))
      .first();

    product = {
      ...product,
      image: image ? image.path : ''
    }

    let obj = {}
    obj[`product_${id}`] = {
      ...product,
      quantity: p_quantity ? Number(p_quantity) : 1
    };

    const is_exist =  !!products.filter((item) => {
      return item[`product_${id}`] !== undefined
    }).length


    if(!is_exist){
      products.push(obj)
    } else {
      products.map((item) => {
        if(item[`product_${id}`] !== undefined){
          item[`product_${id}`].quantity = Number(item[`product_${id}`].quantity) + Number(p_quantity)
        }
        return item;
      })
    }

    let total = 0.00;
    let quantity = 0;
    products.map((product)=>{
      let tmp = Object.entries(product)[0][1];
      quantity = Number(quantity + tmp.quantity);
      total = total + (Number(tmp.price_local) * tmp.quantity)
    })


    session.put('products', products);
    session.put('total', total.toFixed(2));
    session.put('quantity', quantity);
    const cart = View.render('Web.chunks.cart', {
      session: {
        products: session.get('products', []).map((product)=>{
          return Object.entries(product)[0][1]
        }),
        total: session.get('total'),
        quantity: session.get('quantity')
      }
    });


    return response.status(200).json({
      cart,
      success: true
    })
  }

  async delete({params, session, response}){
    const {id} = params;

    let products = session.get('products', []);
    products.splice(products.findIndex(item => item === `product_${id}`), 1)

    let total = 0.00;
    let quantity = 0;
    products.map((product)=>{
      let tmp = Object.entries(product)[0][1];
      total = total + Number(tmp.price_local)
      quantity = Number(quantity + tmp.quantity);
    })
    session.put('quantity', quantity);
    session.put('total', total.toFixed(2));

    const cart = View.render('Web.chunks.cart', {
      session: {
        products: session.get('products', []).map((product)=>{
          return Object.entries(product)[0][1]
        }),
        total: session.get('total'),
        quantity: session.get('quantity')
      }
    });
    return response.status(200).json({
      cart,
      total: session.get('total'),
      success: true
    })
  }

  async changeQuantity({ request, params, session, response}){
    const {id} = params;
    const {p_quantity} = request.all();
    const products = session.get('products', []);

    products.map((item) => {
      if(item[`product_${id}`] !== undefined){
        item[`product_${id}`].quantity = Number(p_quantity)
      }
      return item;
    })


    let total = 0.00;
    let quantity = 0;
    products.map((product)=>{
      let tmp = Object.entries(product)[0][1];
      quantity = Number(quantity + tmp.quantity);
      total = total + (Number(tmp.price_local) * tmp.quantity)
    })

    session.put('products', products);
    session.put('total', total.toFixed(2));
    session.put('quantity', quantity);
    const cart = View.render('Web.chunks.cart', {
      session: {
        products: session.get('products', []).map((product)=>{
          return Object.entries(product)[0][1]
        }),
        total: session.get('total'),
        quantity: session.get('quantity')
      }
    });
    return response.status(200).json({
      cart,
      total: session.get('total'),
      success: true
    })
  }

  async show({ request, session, view }){
    let products = session.get('products', []);
    products = Object.entries(products).map((item) => {
      return Object.entries(item[1])[0][1]
    })
    const meta = await Meta.get(request.url());

    return view.render(products.length ? 'Web.pages.cart' : 'Web.pages.emptyCart', {
      ...meta,
      products,
      slug: request.url()
    });
  }

  async order({ request, session, view }){
    let products = session.get('products', []);
    products = Object.entries(products).map((item) => {
      return Object.entries(item[1])[0][1]
    })
    const meta = await Meta.get(request.url());

    return view.render('Web.pages.order', {
      ...meta,
      products,
      total: session.get('total'),
      slug: request.url()
    });
  }
}

module.exports = CartController;