'use strict';

const Env = use('Env');

module.exports = {
  languages: [{
    name: 'ru',
    label_ru: 'Русский',
    label_ua: 'Украинский'
  }, {
    name: 'ua',
    label_ru: 'Російська',
    label_ua: 'Українська'
  }]
};
