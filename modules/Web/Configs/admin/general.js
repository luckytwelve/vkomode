module.exports = {
  locales: [
    // {
    //   slug: 'en',
    //   title: 'English',
    //   flag: '/admin/assets/media/svg/flags/012-uk.svg',
    //   shortTitle: 'Eng'
    // },
    {
      slug: 'ru',
      title: 'Русский',
      flag: '/admin/assets/media/svg/flags/248-russia.svg',
      shortTitle: 'Рус'
    },
  ],
  defaultLocale: 'ru',
  masterTemplate: 'Adm.index.index'
};
