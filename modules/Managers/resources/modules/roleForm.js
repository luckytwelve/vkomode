const ADM = require('../../../Adm/resources/assets/adm/adm');


ADM.modules.set('roleForm', {
  events: {
    'click: #selectAll': 'selectAll',
  },

  selectAll: function(e, module) {
    e.preventDefault();
    $('.checkbox :checkbox', $(module.el)).each(function() {this.checked = true;});
  },

  init: function(...rest){
    $("#loginForm", this.el).validate({
      rules: {
        email: {
          required: true,
          email: true,
          minlength: 10
        },
        password: {
          required: true,
          minlength: 6
        }
      },
      submitHandler: (form) => {
        this.login(form)
      }
    });
  }
});
