/* global $, window, __ */

const ADM = require('../../../Adm/resources/assets/adm/adm');


ADM.modules.set('users', {
  tableSelector: '#users',

  ReloadTable: (response, module) => {
    $(module.tableSelector).DataTable().ajax.reload(null, false);
  },

  AfterFormSend: (response, module) => {
    if (!response.notification) return;
    if (response.notification.type === 'success') {
      $('.modal').modal('hide');
      $(module.tableSelector).DataTable().ajax.reload(null, false);
    }
  },

  init(...args) {
    this.table = $(this.tableSelector).DataTable({
      scrollY: '50vh',
      scrollX: true,
      scrollCollapse: true,

      processing: true,
      serverSide: true,

      initComplete(oSettings) {
        $(window).resize();
      },

      drawCallback(oSettings) {
        $(window).trigger('resize');
      },

      ajax: {
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: `${ADM.managerPath}/users/list`,
        type: 'POST',
        dataType: 'json',
      },

      columns: [
        {
          data: 'id',
          name: 'users.id'
        },
        {
          data: 'email',
          name: 'users.email',
          orderable: true,
          searchable: true
        },
        {
          data: 'first_name',
          name: 'users.first_name',
          orderable: true,
          searchable: true
        },
        {
          data: 'last_name',
          name: 'users.last_name',
          orderable: true,
          searchable: true
        },
        {
          data: 'slug',
          name: 'roles.slug',
          orderable: true,
          searchable: true
        },
        {
          data: 'actions',
          name: 'actions',
          orderable: false,
          searchable: false,
          render(data, type, row, meta) {
            return `
            <div class="dropdown dropdown-inline">
              <a href="javascript:;" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown" aria-expanded="false">
                <i class="la la-cog"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" style="display: none;">
                <ul class="nav nav-hoverable flex-column">
                  <li class="nav-item"><a class="nav-link no-history" href="${ADM.managerPath}/users/edit/${row.id}"><i class="nav-icon la la-edit"></i><span class="nav-text">${__('Adm.admin.edit')}</span></a></li>
                  ${row.id === 1 ? '' : `
                    <li class="nav-item">
                      <a class="nav-link no-history" href="${ADM.managerPath}/users/delete/${row.id}" data-callback="users::ReloadTable" data-confirm="Are you sure you want to delete item with ID ${row.id}?">
                      <i class="nav-icon la la-trash"></i><span class="nav-text">${__('Adm.admin.remove')}</span>
                      </a>
                    </li>
                  `}
                </ul>
              </div>
            </div>
            `;
          }
        }
      ],

      // initComplete: function () {
      // },

      // drawCallback: function () {
      //   $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
      // },

      // preDrawCallback: function() {
      //   $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
      // }
    });
  }
});
