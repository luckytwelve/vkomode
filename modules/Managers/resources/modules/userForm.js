const ADM = require('../../../Adm/resources/assets/adm/adm');


ADM.modules.set('userForm', {
  events: {
    'click: #selectAll': 'selectAll',
  },

  selectAll: function(e, module) {
    e.preventDefault();
    $('.checkbox :checkbox', $(module.el)).each(function() {this.checked = true;});
  },

  init: function(){
    
  }
});
