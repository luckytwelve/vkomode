const ADM = require('../../../Adm/resources/assets/adm/adm');


ADM.modules.set('roles', {
  scrollY: '50vh',
  scrollX: true,
  scrollCollapse: true,

  tableSelector: '#roles',

  ReloadTable: function (response, module) {
    $(module.tableSelector).DataTable().ajax.reload(null, false);
  },

  AfterFormSend: function (response, module) {
    if (!response.notification) return;
    if (response.notification.type == 'success') {
      $('.modal').modal('hide');
      $(module.tableSelector).DataTable().ajax.reload(null, false);
    }
  },

  init: function () {
    this.table = $(this.tableSelector).DataTable({
      processing: true,
      serverSide: true,

      initComplete: (oSettings) => {
        $(window).resize();
      },

      drawCallback: (oSettings) => {
        $(window).trigger('resize');
      },

      ajax: {
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: ADM.managerPath + '/roles/list',
        type: 'POST',
        dataType: 'json',
      },

      columns: [
        {data: 'id', name: 'roles.id'},
        {data: 'slug', name: 'roles.slug', orderable: true, searchable: true},
        {data: 'name', name: 'roles.name', orderable: true, searchable: true},
        {data: 'description', name: 'roles.description', orderable: true, searchable: true},
        {
          data: 'actions',
          name: 'actions',
          orderable: false,
          searchable: false,
          render: (data, type, row, meta) => {
            return `
            <div class="dropdown dropdown-inline">
              <a href="javascript:;" class="btn btn-sm btn-clean btn-icon" data-toggle="dropdown" aria-expanded="false">
                <i class="la la-cog"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" style="display: none;">
                <ul class="nav nav-hoverable flex-column">
                  <li class="nav-item"><a class="nav-link no-history" href="${ADM.managerPath}/roles/edit/${row.id}"><i class="nav-icon la la-edit"></i><span class="nav-text">${__('Adm.admin.edit')}</span></a></li>
                  ${row.slug !== 'admin' ? '' : `
                    <li class="nav-item">
                      <a class="nav-link no-history" href="${ADM.managerPath}/roles/delete/${row.id}" data-callback="roles::ReloadTable" data-confirm="Are you sure you want to delete item with ID ${row.id}?">
                      <i class="nav-icon la la-trash"></i><span class="nav-text">${__('Adm.admin.remove')}</span>
                      </a>
                    </li>
                  `}
                    
                </ul>
              </div>
            </div>
            `;
          }
        }
      ],

      // initComplete: function () {
      // },

      // drawCallback: function () {
      //   $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
      // },

      // preDrawCallback: function() {
      //   $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
      // }
    });
  }
});
