const Route = use('Route');

Route.group(() => {
  Route
    .any('users', '@provider:Managers/Controllers/UsersController.index')
    .as('admin.users.list')
    .middleware(['managerXHR', 'managerAuth']);

  Route
    .post('users/list', '@provider:Managers/Controllers/UsersController.list')
    .as('admin.users.list')
    .middleware(['managerXHR', 'managerAuth', 'can:users_view']);

  Route
    .post('users/delete/:id', '@provider:Managers/Controllers/UsersController.deleteManager')
    .as('admin.users.delete')
    .middleware(['managerXHR', 'managerAuth', 'can:users_delete']);

  Route
    .post('users/edit/:id?', '@provider:Managers/Controllers/UsersController.edit')
    .as('admin.users.edit', 'can:(users_edit or users_create)')
    .middleware(['managerXHR', 'managerAuth']);

  Route
    .post('users/save', '@provider:Managers/Controllers/UsersController.save')
    .as('admin.users.save', 'can:(users_edit or users_create)')
    .middleware(['managerXHR', 'managerAuth']);


  // Roles
  Route
    .any('roles', '@provider:Managers/Controllers/RolesController.index')
    .as('admin.roles.list')
    .middleware(['managerXHR', 'managerAuth']);

  Route
    .post('roles/list', '@provider:Managers/Controllers/RolesController.list')
    .as('admin.roles.list')
    .middleware(['managerXHR', 'managerAuth', 'can: roles_view']);

  Route
    .post('roles/edit/:id?', '@provider:Managers/Controllers/RolesController.edit')
    .as('admin.roles.edit')
    .middleware(['managerXHR', 'managerAuth', 'can:(roles_edit or roles_create)']);

  Route
    .post('roles/delete/:id', '@provider:Managers/Controllers/RolesController.delete')
    .as('admin.roles.delete')
    .middleware(['managerXHR', 'managerAuth', 'can: roles_delete']);

  Route
    .post('roles/save', '@provider:Managers/Controllers/RolesController.save')
    .as('admin.roles.save')
    .middleware(['managerXHR', 'managerAuth', 'can:(roles_edit or roles_create)']);
}).prefix('admin');
