'use strict'

module.exports = {
  'Admin users':
    [
      {
        'slug': 'users_view',
        'name': 'Admin users view'
      },
      {
        'slug': 'users_create',
        'name': 'Admin users create'
      },
      {
        'slug': 'users_edit',
        'name': 'Admin users edit'
      },
      {
        'slug': 'users_delete',
        'name': 'Admin users delete'
      }
    ],
  'Roles':
    [
      {
        'slug': 'roles_view',
        'name': 'Roles view'
      },
      {
        'slug': 'roles_create',
        'name': 'Roles create/edit'
      },
      {
        'slug': 'roles_edit',
        'name': 'Roles edit'
      },
      {
        'slug': 'roles_delete',
        'name': 'Roles delete'
      }
    ]
}
