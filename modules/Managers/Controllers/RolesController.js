'use strict'

const moment = require('moment');

const Route = use('Route');
const View = use('View');
const Database = use('Database');
const { validate } = use('Validator');
const Role = use('App/Models/Role');
const Permission = use('App/Models/Permission');

const Config = use('Config');
const TableBuilder = use('ADM/TableBuilder');
const Datatables = use('ADM/Datatables');
const Notify = use('ADM/Notify');


class RolesController {

  async index({ view, __, auth }) {
    const table = new TableBuilder('roles');

    table.setName(__('Adm.admin.roles_list'));

    if (await this.checkPermission(auth, 'roles_create') === true) {
      table.setButtons([
        [`<a href="${Route.url('admin.roles.edit')}" class="pull-right btn btn-success">${__('Adm.admin.create')}</a>`]
      ]);
    }

    table.setColums([
      { title: '#', width: '1%' },
      { title: __('Adm.admin.slug'), width: '5%' },
      { title: __('Adm.admin.name'), width: '10%' },
      { title: __('Adm.admin.description'), width: '15%' },
      { title: __('Adm.admin.actions'), width: '1%' }
    ]);

    View.global('breadcrumbs', [
      { name: __('Adm.admin.home'), url: Route.url('admin.dashboard.index') },
      { name: __('Adm.admin.roles'), url: Route.url('admin.roles.list') }
    ]);

    return view.render('Managers.roles.index', { table: table.build() });
  }

  async list({ request, response }) {
    const input = request.all();

    const query = Database
      .select(
        'roles.id',
        'roles.slug',
        'roles.name',
        'roles.description')
      .from('roles');

    const table = new Datatables(query, request);
    const res = await table.make();
    return response.json(res);
  }

  /**
   *
   *
   * @param {*} { request, response, params }
   * @returns
   * @memberof RolesController
   */
  async edit({ request, response, params, antl, __, auth }) {
    const { id } = params;
    const data = {
      role: {},
      rolePermission: []
    };

    data.permissions = Config.get('admin.permissions');

    const translatedPermissions = {};

    for (let key in data.permissions) {
      const groupPermissions = [];

      data.permissions[key].forEach(p => {
        groupPermissions.push({
          ...p,
          name: __(`cms.permissions.${p.slug}`) === `${antl.currentLocale()}.cms.permissions.${p.slug}` ? p.name : __(`cms.permissions.${p.slug}`)
        })
      });
      translatedPermissions[key] = groupPermissions;
    }

    data.permissions = translatedPermissions

    if (id) {
      const role = await Role.find(Number(id));

      if (!role) {
        return response.json(Notify.error('Role does not exist', {}))
      }


      data.role = role.toJSON();
      data.rolePermission = (await role.permissions().fetch()).toJSON().map(p => p.slug);
    }

    return response.json({
      modal: {
        title: id ? __('Adm.admin.edit_role') : __('Adm.admin.create_new_role'),
        content: View.render('Managers.roles.form', data),
        width: 900,
        cancel: __('Adm.admin.cancel'),
        submit: __('Adm.admin.save'),
      },
      success: true
    });
  }

  async save({ request, response }) {
    const input = request.all();
    const isEdit = !!input.id;

    const rules = {
      slug: `required|min:3|max:255|regex:^[a-zA-Z0-9_-]+$${isEdit ? '' : '|unique:roles'}`,
      name: 'required|min:3|max:255|regex:^[a-zA-Z0-9_-]+$',
      description: 'min:3|max:1000',
      permissions: 'required',
    }

    const messages = {
      'permissions.required': 'at least one completed checkbox is required'
    }

    const validation = await validate(input, rules, messages)

    if (validation.fails()) {
      return response.json(Notify.error(validation.messages()[0].message, {}))
    }


    if (!input.id && await Role.query().where('slug', input.slug).getCount() > 0) {
      return response.json(Notify.error('Group with this slug already exists.', {}));
    }

    let role = {}

    if (!input.id) {
      role = new Role()
    } else {
      role = await Role.query().where('id', input.id).first()
    }

    role.name = input.name
    role.slug = input.slug
    role.description = input.description
    role.updated_at = moment().utcOffset(0).unix()
    await role.save()

    const rolePermissions = (await role.permissions().fetch()).toJSON().map(p => p.id)

    if (rolePermissions.length) {
      await role.permissions().detach(rolePermissions);
    }


    const existedPermissions = (await Permission.all()).toJSON();

    if (input.permissions && input.permissions.length) {
      const ids = input.permissions.map(p => existedPermissions.find(e => e.slug === p).id);
      await role.permissions().attach(ids);
    }

    return response.json(Notify.success(`Group was successfully ${input.id == 'undefined' ? 'created' : 'edited'}`, {}))
  }




  async delete({ request, response, params }) {
    const { id } = params;

    let role = await Role.find(Number(id))

    if (!role)
      return response.json(Notify.error('Role not found', {}))
    if (await role.delete())
      return response.json(Notify.success('Deleted', {}))
    else
      return response.json(Notify.error('Something went wrong.', {}))
  }

  async checkPermission(auth, role) {
    const user = await auth.user.getPermissions();
    //console.log('========>>>>', user.includes('roles_create'));
    return user.includes(role);
  }
}

module.exports = RolesController
