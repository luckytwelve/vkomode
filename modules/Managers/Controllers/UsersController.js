'use strict';

const Route = use('Route');
const View = use('View');
const Datatables = use('ADM/Datatables');
const TableBuilder = use('ADM/TableBuilder');
const Database = use('Database');
const { validate } = use('Validator');

const Role = use('App/Models/Role');
const User = use('App/Models/User');
const Notify = use('ADM/Notify');
const moment = require('moment');

class UsersController {
  async index({ view, __, auth }) {
    const permissions = await auth.user.getPermissions();
    const table = new TableBuilder('users');
    table.setName(__('Adm.admin.users_list'));
    if (permissions.indexOf('users_create') !== -1) {
      table.setButtons([
        [`<a href="${Route.url('admin.users.edit')}" class="pull-right btn btn-success">${__('Adm.admin.create')}</a>`]
      ]);
    }

    table.setColums([
      { title: '#', width: '1%' },
      { title: __('Adm.admin.email'), width: '10%' },
      { title: __('Adm.admin.first_name'), width: '15%' },
      { title: __('Adm.admin.last_name'), width: '15%' },
      { title: __('Adm.admin.roles'), width: '15%' },
      { title: __('Adm.admin.actions'), width: '1%' }
    ]);

    View.global('breadcrumbs', [
      { name: __('Adm.admin.home'), url: Route.url('admin.dashboard.index') },
      { name: __('Adm.admin.users_list'), url: Route.url('admin.users.list') }
    ]);

    return view.render('Managers.users.index', {
      table: table.build()
    });
  }

  async list({ request, response }) {
    const query = Database
      .select('users.id',
        'users.email',
        'users.first_name',
        'users.last_name',
        'role_user.role_id',
        'roles.slug')
      .from('users')
      .leftJoin('role_user', 'users.id', 'role_user.user_id')
      .leftJoin('roles', 'roles.id', 'role_user.role_id')
      .where('users.isAdmin', true)
      .orWhere('roles.slug', 'admin');

    const table = new Datatables(query, request);
    const res = await table.make();
    return response.json(res);
  }

  async logout({ auth, response }) {
    await auth.logout();
    response.json({ success: true });
  }

  async edit({
    request, response, params, __
  }) {
    const { id } = params;
    const data = {
      roles: (await Role.all()).toJSON()
    };
    if (id) {
      const user = await User.find(Number(id));

      if (!id) {
        return response.json(Notify.error('User not found', {}));
      }

      data.fields = user.toJSON();
      data.userRoles = (await user.roles().fetch()).toJSON().map(r => r.slug);
    }

    return response.json({
      modal: {
        title: id ? __('Adm.admin.edit_user') : __('Adm.admin.create_new_user'),
        content: View.render('Managers.users.form', data),
        cancel: __('Adm.admin.cancel'),
        submit: __('Adm.admin.save')
      },
      success: true
    });
  }

  async save({ request, response }) {
    const input = request.all();
    const isEdit = !!input.id;

    const rules = {
      email: `${isEdit ? '' : 'required|'}email|unique:users,email,id,${isEdit ? input.id : ''}`,
      first_name: `${isEdit ? '' : 'required|'}min:3|max:255`,
      last_name: `${isEdit ? '' : 'required|'}min:3|max:255`,
      password: `${isEdit ? '' : 'required|'}min:6|max:64|same:repassword`,
      repassword: `${isEdit ? '' : 'required|'}min:6|max:64|same:password`
    };

    const messages = {
      'name.required': 'required validation failed on first name',
      'secondName.required': 'required validation failed on last name',
    };

    const validation = await validate(input, rules, messages);
    if (validation.fails()) {
      return response.json(Notify.error(validation.messages()[0].message, {}));
    }

    let user = {};
    if (!input.id) {
      user = new User();
    } else {
      user = await User.find(Number(input.id));
      if (!user) {
        return response.json(Notify.error('Manager not found', {}));
      }
    }

    user.isAdmin = true;
    user.first_name = input.first_name;
    user.last_name = input.last_name;
    user.email = input.email;
    user.blocked = input.blocked ? 1 : 0;

    if (!isEdit || input.password) {
      user.password = input.password;
    }

    await user.save();

    const roles = (await user.roles().fetch()).toJSON().map(r => r.id);
    await user.roles().detach(roles);

    if (input.roles && input.roles.length) {
      for (let i = 0; i < input.roles.length; i += 1) {
        await user.roles().attach([
          input.roles[i]
        ]);
      }
    }

    return response.json(Notify.success('Saved', {}));
  }

  async deleteManager({ request, response, params }) {
    const { id } = params;
    const user = await User.find(Number(id));

    if (!user) {
      return response.json(Notify.error('Something went wrong. User not found', {}));
    }

    if (user) {
      user.delete();
      return response.json(Notify.success('Deleted', {}));
    }
    return {};
  }
}

module.exports = UsersController;
