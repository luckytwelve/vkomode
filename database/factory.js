'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

function randomInteger(min, max) {
  let rand = min - 0.5 + Math.random() * (max - min + 1)
  rand = Math.round(rand);
  return rand;
}

Factory.blueprint('App/Models/User', async (faker) => {
  return {
    first_name: faker.first(),
    last_name: faker.last(),
    email: faker.email(),
    password: '123qqq',
    avatar: faker.avatar({protocol: 'https', fileExtension: 'jpg'}),
    facebook_payload: {},
    data: {}
  }
})

Factory.blueprint('App/Models/Sport', async (faker) => {
  const sports = [
    'Football',
    'Basketball',
    'Volleyball',
    'Rugby',
    'Polo',
    'Curling',
    'Poker',
    'Tennis',
    'Kickball',
    'Snowboarding',
    'Judo',
    'Sumo',
    'Wrestling',
    'Gouren',
    'Boxing'
  ];
  return {
    title: sports[randomInteger(0,14)],
    icon: faker.avatar({protocol: 'https', fileExtension: 'jpg'})
  }
})

Factory.blueprint('App/Models/Workout', async (faker) => {
  const workouts = [
    'Inchworm',
    'Tuck Jump',
    'Bear Crawl',
    'Mountain Climber',
    'Plyometric Push-Up',
    'Stair Climb with Bicep Curl',
    'Prone Walkout',
    'Burpees',
    'Plank',
    'Plank-to-Push-Up',
    'Wall Sit',
    'Lunge',
    'Clock Lunge',
    'Lunge-to-Row',
    'Pistol Squat',
    'Lunge Jump',
    'Curtsy Lunge',
    'Squat',
    'Single Leg Deadlift',
    'Squat Reach and Jump',
    'Chair Squat Pose',
    'Quadruped Leg Lift',
    'Step-Up',
    'Calf Raise',
  ];
  return {
    sport_id: randomInteger(1,15),
    age_group_id: randomInteger(1,4),
    title: workouts[randomInteger(0, 23)],
    description: faker.paragraph({ sentences: 1 }),
    background: faker.avatar({protocol: 'https', fileExtension: 'jpg'})
  }
})

Factory.blueprint('App/Models/Coach', async (faker) => {
  return {
    workout_id: randomInteger(1,10),
    first_name: faker.first(),
    last_name: faker.last(),
    email: faker.email(),
    description: faker.paragraph({ sentences: 1 }),
    avatar: faker.avatar({protocol: 'https', fileExtension: 'jpg'}),
    background: faker.avatar({protocol: 'https', fileExtension: 'jpg'})
  }
})

Factory.blueprint('App/Models/WorkoutCourse', async (faker) => {
  return {
    workout_id: randomInteger(1,10),
    paid_course: randomInteger(0,1),
    title: faker.word(),
    description: faker.paragraph({ sentences: 1 }),
    background: faker.avatar({protocol: 'https', fileExtension: 'jpg'})
  }
})


Factory.blueprint('App/Models/Training', async (faker) => {
  const video = [
    'https://www.youtube.com/watch?v=nX7Mt6rccls',
    'https://www.youtube.com/watch?v=DWai4qCxr9s',
    'https://www.youtube.com/watch?v=--VcKVVLdQA',
    'https://www.youtube.com/watch?v=7CU7JrHwgb4',
    'https://www.youtube.com/watch?v=bzJUuamNTNY',
    'https://www.youtube.com/watch?v=fTy7uxEP7z0',
    'https://www.youtube.com/watch?v=Fd3dxjLEfq8',
    'https://www.youtube.com/watch?v=IONOCwZug-0',
    'https://www.youtube.com/watch?v=8gVd6jhWEFc',
    'https://www.youtube.com/watch?v=yi0hwFDQTSQ'
  ];

  return {
    course_id: randomInteger(1,10),
    title: faker.word(),
    description: faker.paragraph({ sentences: 1 }),
    background: faker.avatar({protocol: 'https', fileExtension: 'jpg'}),
    video_link: video[randomInteger(0,9)]
  }
})

