'use strict';

const moment = require('moment');

const Hash = use('Hash');
const Database = use('Database');
const Factory = use('Factory');

class UsersSeeder {
  async run() {
    await Database.table('users')
      .insert({
        email: 'admin@admin.com',
        password: await Hash.make('123qqq'),
        first_name: 'Admin',
        last_name: 'Admin',
        isAdmin: true,
        created_at: moment().format(),
        updated_at: moment().format()
      });
    await Database.table('users')
      .insert({
        email: 'manager@manager.com',
        password: await Hash.make('123qqq'),
        first_name: 'Manager',
        last_name: 'Manager',
        isAdmin: true,
        created_at: moment().format(),
        updated_at: moment().format()
      });
  }
}

module.exports = UsersSeeder;