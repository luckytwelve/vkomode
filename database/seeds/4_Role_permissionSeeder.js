'use strict';

const Factory = use('Factory');
const Database = use('Database');

class RolePermissionSeeder {
  async run() {
    const count = await Database
      .from('permissions')
      .count();
    for (let i = 1; i < count[0].count; i += 1) {
      await Database.table('permission_role')
        .insert(
          {
            permission_id: i,
            role_id: 1,
          }
        );
    }
  }
}

module.exports = RolePermissionSeeder;