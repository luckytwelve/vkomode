'use strict';

const Database = use('Database');
const moment = require('moment');

const Factory = use('Factory');

class RolesSeeder {
  async run() {
    await Database.table('roles')
      .insert({
        slug: 'admin',
        name: 'admin',
        description: 'admin',
        created_at: moment().format(),
        updated_at: moment().format()
      });

    await Database.table('roles')
      .insert({
        slug: 'manager',
        name: 'manager',
        description: 'manager',
        created_at: moment().format(),
        updated_at: moment().format()
      });
  }
}

module.exports = RolesSeeder;