'use strict';

const Database = use('Database');
const moment = require('moment');

const Factory = use('Factory');

class RoleUserSeeder {
  async run() {
    const users = await Database.table('users').whereIn('email', ['admin@admin.com', 'manager@manager.com']);

    await Database.table('role_user')
      .insert({
        role_id: 1,
        user_id: users[0].id,
        created_at: moment().format(),
        updated_at: moment().format()
      });
    await Database.table('role_user')
      .insert({
        role_id: 2,
        user_id: users[1].id,
        created_at: moment().format(),
        updated_at: moment().format()
      });
  }
}

module.exports = RoleUserSeeder;