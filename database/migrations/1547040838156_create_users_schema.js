'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CreateUsersSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('phone')
      table.string('username')
      table.string('first_name')
      table.string('last_name')
      table.string('email')
      table.string('password', 60)
      table.boolean('blocked');
      table.string('avatar')
      table.integer('timezone')
      table.boolean('isAdmin').defaultTo(false);
      table.integer('role')
      table.integer('is_blocked').nullable()
      table.integer('is_charger').nullable()
      table.integer('deleted_at').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = CreateUsersSchema
