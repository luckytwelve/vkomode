'use strict'

const Schema = use('Schema');

class CategoriesSchema extends Schema {
  up() {
    this.create('categories', (table) => {
      table.increments();
      table.integer('parent_id').nullable();
      table.string('slug', 60);
      table.text('title');
      table.text('description');
      table.integer('order').nullable(0);
      table.boolean('active').defaultTo(false);
      table.json('meta').nullable()
      table.timestamps();
    });
  }

  down() {
    this.drop('categories');
  }
}

module.exports = CategoriesSchema;
