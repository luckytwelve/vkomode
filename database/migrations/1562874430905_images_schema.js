'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class ImagesSchema extends Schema {
  up() {
    this.create('images', (table) => {
      table.increments();
      table.integer('entity_id').unsigned().index();
      // table.foreign('_id').references('id').on('goods').onDelete('cascade')
      table.string('path', 250);
      table.string('title', 250);
      table.string('entity', 20);
      table.string('status', 20);
      table.timestamps();
    });
  }

  down() {
    this.drop('images');
  }
}

module.exports = ImagesSchema;
