'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SeoSchema extends Schema {
  up () {
    this.create('seo', (table) => {
      table.increments()
      table.string('slug')
      table.string('meta_title')
      table.text('meta_description')
      table.text('meta_keywords')
      table.timestamps()
    })
  }

  down () {
    this.drop('seos')
  }
}

module.exports = SeoSchema
