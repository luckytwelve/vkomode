
const Schema = use('Schema');

class ProductModificationsSchema extends Schema {
  up() {
    this.create('product_mods', (table) => {
      table.increments();
      table.integer('product_id').notNullable();
      table.string('sku', 20);
      table.json('data');
      table.integer('available').defaultTo(0);
      table.timestamps();
    });
  }

  down() {
    this.drop('product_mods');
  }
}

module.exports = ProductModificationsSchema;
