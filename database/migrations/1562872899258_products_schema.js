'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductsSchema extends Schema {
  up() {
    this.create('products', (table) => {
      table.increments();
      table.string('title').notNullable();
      table.string('slug').notNullable();
      table.text('description');
      table.decimal('price_base', 9, 2);
      table.decimal('price_local', 9, 2);
      table.integer('quantity').unsigned();
      table.string('art');
      table.json('attributes').nullable();
      table.boolean('active').defaultTo(false);
      table.boolean('special_offer').defaultTo(false);
      table.boolean('best_product').defaultTo(false);
      table.json('meta').nullable();
      table.timestamps();
    });
  }

  down() {
    this.drop('products');
  }
}

module.exports = ProductsSchema
