'use strict'

const Schema = use('Schema');

class ArticlesSchema extends Schema {
  up() {
    this.create('articles', (table) => {
      table.increments();
      table.string('slug').notNullable().unique();
      table.text('title').nullable();
      table.text('keywords').nullable();
      table.text('short_description').nullable();
      table.text('description').nullable();
      table.text('content').nullable();
      table.text('code').nullable();
      table.boolean('active').defaultTo(false);
      table.string('parent');
      table.timestamps();
    });
  }

  down() {
    this.drop('articles');
  }
}

module.exports = ArticlesSchema;
