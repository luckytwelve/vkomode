'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CreateSettingsSchema extends Schema {
  up () {
    this.create('settings', (table) => {
      table.increments()
      table.string('name')
      table.text('value')
    })
  }

  down () {
    this.drop('settings')
  }
}

module.exports = CreateSettingsSchema
